import numpy as np
from uncertainties import unumpy as unp
import uncertainties as uncert
import uncertainties.umath as umath

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *

def linear(x, a, b):
    return a*x + b

latex_file = Document()

# Variabili globali
I2B = 7.80e-4

d_scala_back = de2unc(1.2, 0.1, 0.)
d_scala_front = de2unc(0.3, 0.1, 0.)
d_bobine = de2unc(17.2, 0.1, 0.)

# calibrazione davanti
cm, px, py = np.loadtxt('dati/calib-davanti.txt', unpack = True)
cm = de2unc(cm, 0., 0.)
px = de2unc(px, 2., 0.)
A, B = ucurve_fit(linear, cm, px, absolute_sigma = True)
chiquadro, ndof = chi2(linear, (A,B,), cm, px)
latex_file.setvariable(Decimal('chiquadrodavanti', chiquadro))
latex_file.setvariable(Decimal('ndofdavanti', ndof, digits = 2))
p2cm_front = 1 / A
sprint(p2cm_front)
residual_plot(
    f = linear,
    param = (A, B),
    X = cm,
    Y = px,
    use_ux = False,
    figfile = 'calib-dav.pdf',
    xlabel = 'd[\\si{\\centi\\meter}]',
    ylabel = 'd[\\texttt{pixel}]',
)

# calibrazione dietro
cm, px, py = np.loadtxt('dati/calib-dietro.txt', unpack = True)
cm = de2unc(cm, 0., 0.)
px = de2unc(px, 1., 0.)
A, B = ucurve_fit(linear, cm, px, absolute_sigma = True)
chiquadro, ndof = chi2(linear, (A,B,), cm, px)
latex_file.setvariable(Decimal('chiquadrodietro', chiquadro))
latex_file.setvariable(Decimal('ndofdietro', ndof, digits = 2))
p2cm_back = 1 / A
sprint(p2cm_back)
residual_plot(
    f = linear,
    param = (A, B),
    X = cm,
    Y = px,
    use_ux = False,
    figfile = 'calib-die.pdf',
    xlabel = 'd[\\si{\\centi\\meter}]',
    ylabel = 'd[\\texttt{pixel}]',
)

# calibrazione complessiva
p2cm = p2cm_front + (d_scala_front + d_bobine/2.)/(d_scala_back+d_scala_front+d_bobine) * (p2cm_back-p2cm_front)
sprint(p2cm)
latex_file.setvariable(UDecimal('pxtocmdavanti', p2cm_front))
latex_file.setvariable(UDecimal('pxtocmdietro', p2cm_back))
latex_file.setvariable(UDecimal('pxtocm', p2cm))

def triangle2radius(a_x, b_x, c_x, a_y, b_y, c_y):
    area = abs((a_x*(b_y-c_y)+b_x*(c_y-a_y)+c_x*(a_y-b_y))/2.)
    l_a = np.sqrt((b_x-c_x)**2+(b_y-c_y)**2)
    l_b = np.sqrt((a_x-c_x)**2+(a_y-c_y)**2)
    l_c = np.sqrt((a_x-b_x)**2+(a_y-b_y)**2)

    return l_a*l_b*l_c/(4*area)

def find_radius(id):
    print("IMMAGINE "+ id)
    # pixel, cm = np.loadtxt('dati/foto_'+id+'_calib.txt', unpack = True)
    # pixel = de2unc(pixel, 1, 0.) #TODO: scegli l'incertezza sui pixel
    # cm = de2unc(cm, 0.05, 0.) #TODO: scegli l'incertezza sulla scala millimetrata

    xm, ym = np.loadtxt('dati/img'+id+'-data-min.txt', unpack = True)
    xM, yM = np.loadtxt('dati/img'+id+'-data-max.txt', unpack = True)

    # Con 3 punti
    R_tm = triangle2radius(*xm[:3], *ym[:3])
    R_tM = triangle2radius(*xM[:3], *yM[:3])
    #R_t = uncert.ufloat((R_tm+R_tM)/2., (R_tM-R_tm)/2.) * p2cm
    #sprint(R_t)

    # Con tutti i punti
    X_mc, Y_mc, R_fm = circle_fit(xm, ym)
    X_Mc, Y_Mc, R_fM = circle_fit(xM, yM)
    upsi_m = relative_difference_circle_fit(xm, ym, X_mc, Y_mc, R_fm)
    upsi_M = relative_difference_circle_fit(xM, yM, X_Mc, Y_Mc, R_fM)
    #R_f = uncert.ufloat((Rm + RM)/2., (RM - Rm)/2.) * p2cm
    #sprint(R_f)
    return R_tm, R_tM, R_fm, R_fM, upsi_m, upsi_M, len(xm), len(xM)

def e_over_m(R, I, V): #I in ampere, radius in cm and V in volt
    return 2*V/(I2B*I*R/100.)**2

met1 = TabularContent()
met2 = TabularContent()

#leggo i dataset
id = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10']
I, V = np.loadtxt('dati/IB.txt')
I_coil = de2unc(I, 0.02, 1.)
V_acc = de2unc(V, 1., 0.)

B = I2B * I_coil
Bt = unp.sqrt(B**2+(4e-5)**2)
print('Test if the MF of Earth is neglible: {} vs {}'.format(B[0],Bt[0]))

R_tm, R_tM, R_fm, R_fM, upsi_m, upsi_M, Nmin, Nmax = map(np.array, zip(*[find_radius(i) for i in id]))

R_t = unarray((R_tm+R_tM)/2., (R_tM-R_tm)/2.) * p2cm
R_f = unarray((R_fm + R_fM)/2., (R_fM - R_fm)/2.) * p2cm

print(((R_tm+R_tM)/2.* p2cm.n))

em_t = e_over_m(R_t, I_coil, V_acc)
em_f = e_over_m(R_f, I_coil, V_acc)

met1.add_column(R_tm * p2cm)
met1.add_column(R_tM * p2cm)
met1.add_column(R_t)
met1.add_column(2*V_acc)
met1.add_column((I2B*I*R_t)**2)
met1.add_column(em_t)

met2.add_column(R_fm * p2cm)
met2.add_column(1.-upsi_m)
met2.add_column(Nmin)
met2.add_column(R_fM * p2cm)
met2.add_column(1.-upsi_M)
met2.add_column(Nmax)
met2.add_column(R_f)
met2.add_column(2*V_acc)
met2.add_column((I2B*I*R_f)**2)
met2.add_column(em_f)

met1.save('tabella1.tmp.tex')
met2.save('tabella2.tmp.tex')

sprint(em_t)
sprint(em_f)

print('DATI SINGOLI')
def cost(x,em):
    return [em] * len(x)
x = de2unc(np.array([i for i in range(1,1+len(id))]), 0., 0.)
print('3 punti')
em, = ucurve_fit(cost, x, em_t, p0 = (1.7e11,))
sprint(em)
latex_file.setvariable(UDecimal('emTrePuntiMedia', em))
residual_plot(
    f = cost,
    param = (em,),
    X = R_t,
    Y = em_t,
    use_ux = False,
    figfile = 'media-em-t.pdf',
    xlabel = '$r [\\si{\\centi\\meter}]$',
    ylabel = '$\\frac{e}{m} [\\si{\coulomb\per\\kilogram}]$',
)

print('fit')
em, = ucurve_fit(cost, x, em_f, p0 = (1.7e11,))
sprint(em)
latex_file.setvariable(UDecimal('emFitMedia', em))
residual_plot(
    f = cost,
    param = (em,),
    X = R_f,
    Y = em_f,
    use_ux = False,
    figfile = 'media-em-f.pdf',
    xlabel = '$r [\\si{\\centi\\meter}]$',
    ylabel = '$\\frac{e}{m} [\\si{\coulomb\per\\kilogram}]$',
)


print('FIT')
print('3 punti')
X_t = (I2B*(R_t)*I_coil)**2
Y_t = 2*V_acc
em, = iterated_fit(lambda x, a: a*x, X_t, Y_t, df = lambda x, a: [a]*len(x))
chiquadro, ndof = chi2iterated(lambda x, a: a*x, (em,), X_t, Y_t, df = lambda x, a: [a]*len(x))
latex_file.setvariable(Decimal('chiquadrtrepunti', chiquadro))
latex_file.setvariable(Decimal('ndoftrepunti', ndof, digits = 2))
latex_file.setvariable(UDecimal('emTrePuntiFit', em * 1e4)) #conversion cm -> m
residual_plot(
    f = lambda x, a: a*x,
    param = (em,),
    X = X_t,
    Y = Y_t,
    outliers = None,
    xlogscale = False,
    use_ux = True,
    df = lambda x, a: [a]*len(x),
    xlabel = '$(B_{max} r)^2 [\\si{\\centi\\meter^2\\tesla^2}]$',
    ylabel = '$2 V_{acc} [\\si{\\volt}]$',
    figfile = "TrePuntiFit.pdf",
)
sprint(em)

print('fit')
X_f = (I2B*(R_f)*I_coil)**2
Y_f = 2*V_acc
em, = iterated_fit(lambda x, a: a*x, X_f, Y_f, df = lambda x, a: [a]*len(x))
chiquadro, ndof = chi2iterated(lambda x, a: a*x, (em,), X_f, Y_f, df = lambda x, a: [a]*len(x))
latex_file.setvariable(Decimal('chiquadrfit', chiquadro))
latex_file.setvariable(Decimal('ndoffit', ndof, digits = 2))
latex_file.setvariable(UDecimal('emFitFit', em * 1e4)) #conversion cm -> m
residual_plot(
    f = lambda x, a: a*x,
    param = (em,),
    X = X_f,
    Y = Y_f,
    outliers = None,
    xlogscale = False,
    use_ux = True,
    df = lambda x, a: [a]*len(x),
    xlabel = '$(B_{max} r)^2 [\\si{\\centi\\meter^2\\tesla^2}]$',
    ylabel = '$2 V_{acc} [\\si{\\volt}]$',
    figfile = "FitFit.pdf",
)
sprint(em)

## Proviamo a considerare il bulbo
def sin_gamma(theta, r, R, D, n1, n2):
    sin = unp.sin(theta)
    cos = unp.cos(theta)
    DR = D/R
    Dr = D/r
    n = n1/n2
    fact1 = unp.sqrt((1.-(n*Dr*sin)**2)*(1.-(DR*sin)**2)) - n*(Dr*DR)*sin**2
    fact2 = cos * unp.sqrt(1.-(n*DR*sin)**2) - n*DR*sin**2
    fact3 = n*DR*sin*cos + sin*unp.sqrt(1.-(n*DR*sin)**2)
    fact4 = n*Dr*sin*unp.sqrt(1.-(DR*sin)**2) + DR*sin*unp.sqrt(1.-(n*Dr*sin)**2)
    return fact1 * fact2 + fact3 * fact4

def cos_gamma(theta, r, R, D, n1, n2):
    return unp.sqrt(1.-(sin_gamma(theta, r, R, D, n1, n2))**2)

def delta_h(theta, r, R, D, n1, n2):
    """
    raggio_reale -  raggio_visto
    """
    return D*unp.sin(theta)/(D/r*cos_gamma(theta, r, R, D, n1, n2)*unp.sin(theta)+unp.sqrt(1.-(D/r*unp.sin(theta))**2)*sin_gamma(theta, r, R, D, n1, n2)) - D*unp.tan(theta)

n1 = 1.
n2 = 1.458
L = d_scala_back+d_bobine/2.
B = d_scala_front + d_bobine/2.
D = (p2cm_front*L+B*p2cm_back)/(p2cm_back-p2cm_front)
latex_file.setvariable(UDecimal('Distanzafotocamera', D))
latex_file.setvariable(Decimal('ifvetro', n2))
r = 5.5
R = 6.0
latex_file.setvariable(Decimal('rinterno', r))
latex_file.setvariable(Decimal('Resterno', R))
theta = unp.arctan(R_f/D)
dh = delta_h(theta, r, R, D, n1, n2)
bulbo = TabularContent()
bulbo.add_column(R_f)
bulbo.add_column(dh)
bulbo.save('tabellabulbo.tmp.tex')

# cose finali
latex_file.save('variabili.tmp.tex')
