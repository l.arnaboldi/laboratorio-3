import numpy as np
from scipy.optimize import least_squares, curve_fit
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath

from LabTools.utils import sprint, de2unc, unpack_unarray
from LabTools.latex import Document, TabularContent, UDecimal, Decimal
from LabTools.plot import residual_plot
from LabTools.fit import ucurve_fit, iterated_fit, chi2

def functionI(V, V0, a, b, I0, alpha):
    return a*np.heaviside(V0-V, 0.)*abs(V-V0)**alpha + b*V +  I0

def derivativeI(V, V0, a, b, I0, alpha):
    return a*(alpha)*np.heaviside(V0-V, 0.)*abs(V-V0)**(alpha-1.)+ b

lambdas = [450, 499, 546, 577]

# Guessed initial parametres
gip = (1.2, 500., -10., 5., 3./2.)
p0 = {l : gip for l in lambdas}
bounds = ([0.,0.,-500.,-500., 1.],[2.,5000.,500.,500.,4.]) # Bounds on parametrs

## Load the data
data = {}
pre_result = {}
ndof = 0
for l in lambdas:
    v, dv, i, di = np.loadtxt('dati/{}.txt'.format(l), unpack = True)
    ndof += len(v)
    V = de2unc(v, dv)
    I = de2unc(i, di)
    data[l] = [V,I]
    p, pcov = curve_fit(functionI, v, i, p0 = p0[l], bounds=bounds) # prefit
    pre_result[l] = p
    print('Pre fit on {}: {}'.format(l, p))

# Auxiliar function
def params2values(params):
    """
    params: (V_0..., a_i..., b, I0, alpha)
    """
    V0 = dict(zip(lambdas, params[:len(lambdas)]))
    a = dict(zip(lambdas, params[len(lambdas):2*len(lambdas)]))
    b = params[2*len(lambdas)]
    I0 = params[2*len(lambdas) + 1]
    alpha = params[2*len(lambdas) + 2]
    return V0, a, b, I0, alpha

# Residual function
def residual(params):
    """
    We expect that measures are scorrelated, so the covariance materix is diagonal
    and we can treat all residual indipendently.
    """
    V0, a, b, I0, alpha = params2values(params)
    residual = {}
    for l in lambdas:
        x, ux = unpack_unarray(data[l][0])
        y, uy = unpack_unarray(data[l][1])
        sigma = np.sqrt(uy**2 + (derivativeI(x, V0[l], a[l], b, I0, alpha) * ux)**2)
        r = (y-functionI(x, V0[l], a[l], b, I0, alpha))/sigma
        residual[l] = r
    return np.array([j for i in residual.values() for j in i])


## Multi fit
initial_condition = (*[pre_result[l][0] for l in lambdas], # V0
                     *[pre_result[l][1] for l in lambdas], # a
                     sum([pre_result[l][2] for l in lambdas])/len(lambdas),
                     sum([pre_result[l][3] for l in lambdas])/len(lambdas),
                     sum([pre_result[l][4] for l in lambdas])/len(lambdas),
                    )
multi_bounds = ([bounds[0][0]]*len(lambdas)+[bounds[0][1]]*len(lambdas)+bounds[0][2:],
                [bounds[1][0]]*len(lambdas)+[bounds[1][1]]*len(lambdas)+bounds[1][2:],)
result = least_squares(residual, initial_condition, bounds=multi_bounds)
J = result.jac #jacobian
cov = np.linalg.inv((J.T).dot(J)) # Approximation of the covariance matrix
params = unc.correlated_values(result.x, cov)
V0, a, b, I0, alpha = params2values(params)

# Plot the data
for l in lambdas:
    residual_plot(
        functionI,
        #unarray(p0[l],[0.]*len(p0[l])),#
        (V0[l], a[l], b, I0, alpha),
        data[l][0],
        data[l][1],
        use_ux = True,
        df = derivativeI,
        figfile= '{}.pdf'.format(l),
        xlabel = '$V[\\si{\\volt}]$',
        ylabel = '$I[\\si{\\pico\\ampere}]$',
    )

## Fit for h/e
def fit_function(lam, C, beta):
    return C/(lam/1e9) - beta # convert lambda in meters

def d_fit_function(lam, C, beta):
    return -C/(lam/1e9)**2 # convert lambda in meters

def linear(freq, he, c):
    return he*(freq*1e12)-c # convert freq in Hz from THz

V0_ = np.array([i for i in V0.values()])
L = de2unc(np.array(lambdas), 5./3, 0.)

C, beta = ucurve_fit(fit_function, L, V0_)
sprint(C)

chi2, ndof_final = chi2(fit_function, (C, beta), L, V0_)

residual_plot(
    fit_function,
    (C, beta),
    X=L,
    Y=V0_,
    use_ux = False,
    figfile= 'final-fit.pdf',
    xlabel = '$\\lambda[\\si{\\nano\\meter}]$',
    ylabel = '$V_0[\\si{\\volt}]$',
)

c = 299792458. # speed of light in m/s
freq = c/L/1e3 #frequence in THz
residual_plot(
    linear,
    (C/c, beta),
    X=freq,
    Y=V0_,
    use_ux = False,
    figfile= 'final-fit-retta.pdf',
    xlabel = '$\\nu[\\si{\\tera\\hertz}]$',
    ylabel = '$V_0[\\si{\\volt}]$',
)

## LaTex part
latex_file = Document()

# Saving fit results
multi_fit = TabularContent()
multi_fit.add_column(L)
multi_fit.add_column(freq)
multi_fit.add_column(V0_)
multi_fit.add_column(np.array([i for i in a.values()]))
multi_fit.save('multi-fit.tmp.tex')

latex_file.setvariable(UDecimal('alphafit', alpha))
latex_file.setvariable(UDecimal('Izero', I0))
latex_file.setvariable(UDecimal('bfit', b))

latex_file.setvariable(UDecimal('hce', C))
latex_file.setvariable(UDecimal('wzeroe', beta))
latex_file.setvariable(Decimal('chiquadro', chi2))
latex_file.setvariable(Decimal('ndofinal', ndof_final, digits = 1))

latex_file.setvariable(Decimal('cost', result.cost))
latex_file.setvariable(Decimal('ndof', ndof, digits = 3))

# cose finali
latex_file.save('variabili.tmp.tex')
