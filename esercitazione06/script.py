# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *


#cose generali
latex_file = Document()


#Parte A
print('________PARTE A________')

data, fond = np.loadtxt('dati/componenti1.txt', unpack = True)
C1 = de2unc(data[0], 3e-10, 4.0)
CT = de2unc(data[1], 3e-12, 4.0)
CF = de2unc(data[2], 3e-12, 4.0)
R1 = de2unc(data[3], 100, 0.8)
R2 = de2unc(data[4], 0.3, 0.8)
Rp = de2unc(data[5], 100., 0.8) #resistenza totale del potenziometro
Vsl = de2unc(data[6], 0., 3) #Non sono sicura dell'incertezza dell'oscilloscopio
Vs = de2unc(3.0, 0., 3.) #Non l'abbiamo misurata ma ok
latex_file.setvariable(UDecimal('RunoA', R1/1000.)) #in kOhm
latex_file.setvariable(UDecimal('RdueA', R2/1000.)) #in kOhm
latex_file.setvariable(UDecimal('CunoA', C1*1.e9))  #in nFarad
latex_file.setvariable(UDecimal('CTA', CT*1.e9))    #in nFarad
latex_file.setvariable(UDecimal('CFA', CF*1.e9))    #in nFarad
latex_file.setvariable(UDecimal('Vsoglia', Vsl*1000)) #in mV
sprint(R1)
sprint(R2)
sprint(C1)
sprint(CT)
sprint(CF)

print()

#Calolo T_alto atteso

T_alto_att = R1 * CF * unp.log(2 * CT * Vs / (CF * Vsl))
latex_file.setvariable(UDecimal('Taltoatt', T_alto_att * 1000.)) #in millisecondi
sprint(T_alto_att)
print()

#TODO: Sistemare la abs(V_soglia), ha fluttuazioni negative e senza abs non converge il fit
#   Implementare gli outliers

def punto1c(txt, pdf, nome):
    #analisi con i dati di Veronica => t = tempo per cui si ha il plateu alto.
    v_in, t = np.loadtxt('dati/' + txt, unpack = True)
    t = de2unc(t, (50.004e-6) / 250., 0.01)
    V_in = de2unc(v_in, 0., 3.)

    #butto il primo punto: TODO: MEDITA SU QUESTA COSA!!!
    #V_in = V_in[1:]
    #t = t[1:]

    #salvo i dati in tabella
    tab1 = TabularContent()
    tab1.add_column(V_in)
    tab1.add_column(t*1e3)
    tab1.save('tabella' + nome + '.tmp.tex')

    #Fit
    def Tout(x, tau, soglia):
        assert(x.all() > 0.)
        #assert(soglia >0.)
        return tau * unp.log(x / abs(soglia))


    def DTout(x, tau, soglia):
        return tau / x

    #best1 = ucurve_fit(Tout, V_in, t, p0 = ((CF * R1).n, Vsl.n))
    #chi1, ndof1 = chi2(Tout, best1, V_in, t)
    best1 = iterated_fit(Tout, V_in, t, df = DTout)
    chi1, ndof1 = chi2iterated(Tout, best1, V_in, t, df = DTout)
    tau1, soglia1 = best1


    #Stampo i parametri di best fit
    sprint(tau1)
    maurizio = abs(soglia1 * CT / CF)
    sprint(maurizio)
    sprint(chi1)
    sprint(ndof1)
    #E li salvo come variabili latex
    latex_file.setvariable(UDecimal('tau' + nome, tau1*1e6))
    latex_file.setvariable(UDecimal('soglia' + nome, maurizio*1e3))
    latex_file.setvariable(Decimal('chisquared' + nome, chi1))
    latex_file.setvariable(Decimal('ndof' + nome, ndof1))

    residual_plot(
        f = lambda x, *args: 1000. * Tout(x, *args),
        df = lambda x, *args: 1000. * DTout(x, *args),
        param = best1,
        #outliers = [True] * 2 + [False] * (len(V_in) - 2),
        X = V_in,
        Y = t * 1000., #in millisecondi
        xlogscale = True,
        use_ux = True,
        xlabel = '$2V_S$ [$\\si{\\volt}$]',
        ylabel = '$\\Delta t_\\text{alto}$ [$\\si{\\milli\\second}$]',
        figfile = pdf,
    )

#Analisi con i dati di Veronica
print('Fit Veronica')

punto1c('1cveronica.txt', '1cveronica.pdf', 'Ver')

print()

#Analisi con i dati di Luca
print('Fit Luca')

punto1c('1c.txt', '1cluca.pdf', 'Luca')

print()
print('Valori attesi parte 1c')
#Calcolo i valori attesi per questa parte

tau_att = R1 * CF
sprint(tau_att)
sprint(Vsl)

latex_file.setvariable(UDecimal('tauatt', tau_att * 1e6))    #in microsecondi

#Parte B

print('________PARTE B________')

#Leggo i valori dei componenti
data, fond = np.loadtxt('dati/giornodopo/resistenze.txt', unpack = True)
R1 = de2unc(data[0], 10., 0.8)
R2 = de2unc(data[1], 10., 0.8)
R3 = de2unc(data[2], 1., 0.8)
R = de2unc(data[3], 10., 0.8)
Rbasso = de2unc(data[4], 1., 0.8)
latex_file.setvariable(UDecimal('RunoB', R1/1000.)) #in kOhm
latex_file.setvariable(UDecimal('RdueB', R2/1000.)) #in kOhm
latex_file.setvariable(UDecimal('RtreB', R3/1000.)) #in kOhm
latex_file.setvariable(UDecimal('RB', R/1000.)) #in kOhm
latex_file.setvariable(UDecimal('RbassoB', Rbasso/1000.)) #in kOhm
sprint(R1)
sprint(R2)
sprint(R3)
sprint(R)
sprint(Rbasso)


data, fond = np.loadtxt('dati/giornodopo/componenti.txt', unpack = True)
C = de2unc(data[0], 3e-9, 4.0)
sprint(C)
latex_file.setvariable(UDecimal('CB', C * 1.e9)) #in nanoFarad

Tquad = 2 * R * C * unp.log(1 + 2 * R2 / R1)
sprint(Tquad)
latex_file.setvariable(UDecimal('TquadB', Tquad * 1e3)) #in millisecondi

Vpiu_att = 13.8 / (1 + R1 / R2)
sprint(Vpiu_att)
latex_file.setvariable(UDecimal('Vpiuatt', Vpiu_att))
Ie = (15.0 - 3.44) / R3
sprint(Ie)

#punto 2e
v_cc, v_ee, f = np.loadtxt('dati/giornodopo/2e.txt', unpack = True)
V_CC = de2unc(v_cc, 0., 3.)
t = de2unc(1. / f, (250.004e-6) / 250., 0.01)
f = 1. / t


alpha = 1. + R3 / (R1 + R2)
beta = 1. / (1. + R1 / R2)
tau = R * C * (1. + R3 / (R * alpha))

#Tquade = tau * unp.log(2 * (beta - R3 * C / (alpha * tau)) / (beta - 1.) - 1 )
cosaacaso = 2 * (beta - R3 * C / (alpha * tau)) / (beta - 1.) - 1 
#fquade = 1. / Tquade
sprint(cosaacaso)

errorbars_plot(
    X=V_CC,
    Y= f,
    xlabel = '$V_{CC}$ [$\\si{\\volt}$]',
    ylabel = '$f$ [$\\si{\\hertz}$]',
    figfile = '2efrequenzaampiezza.pdf',
)

print ()
#punto 2f
CIN, fondo, ff = np.loadtxt('dati/giornodopo/2f.txt', unpack = True)
tt = de2unc(1. / ff, (2.504e-6) / 250., 0.01)
ff = 1. / tt
Cin = unp.uarray(CIN, CIN)
#per i dati originali usiamo questo
Cin[0] = de2unc(CIN[0], 3e-9, 4.0)
Cin[1] = de2unc(CIN[1], 3e-10, 4.0)
Cin[2] = de2unc(CIN[2], 3e-10, 4.0)
Cin[3] = de2unc(CIN[3], 3e-10, 4.0)
Cin[4] = de2unc(CIN[4], 3e-12, 4.0)
Cin[5] = de2unc(CIN[5], 3e-12, 4.0)
Cin[6] = de2unc(CIN[6], 3e-12, 4.0)
#Cin[7] = de2unc(CIN[7], 3e-12, 4.0)

"""
Cin[0] = de2unc(CIN[0], 3e-9, 4.0)
#Cin[1] = de2unc(CIN[1], 3e-10, 4.0)
Cin[1] = de2unc(CIN[1], 3e-10, 4.0)
Cin[2] = de2unc(CIN[2], 3e-10, 4.0)
Cin[3] = de2unc(CIN[3], 3e-12, 4.0)
Cin[4] = de2unc(CIN[4], 3e-12, 4.0)
Cin[5] = de2unc(CIN[5], 3e-12, 4.0)
#Cin[6] = de2unc(CIN[6], 3e-12, 4.0)
"""

def Tau(x, k):
    return k / x

def DTau(x, k):
    return - k / x**2

bestf = iterated_fit(Tau, Cin, ff, df = DTau)
chi2f, ndof2f = chi2iterated(Tau, bestf, Cin, ff, df = DTau)

Rf = 1. /(2. * unp.log(1. + 2. * R2 / R1) * bestf[0])
sprint(Rf)
sprint(chi2f)
sprint(ndof2f)

latex_file.setvariable(UDecimal('Rpuntoduef', Rf / 1000.)) #in kOhm
latex_file.setvariable(Decimal('chisquaredduef', chi2f))
latex_file.setvariable(Decimal('ndofduef', ndof2f))

residual_plot(
        f= Tau,
        df = DTau,
        param = bestf,
        #outliers = [False] * (len(Cin) - 1) + [True],
        X=Cin,
        Y=ff,
        xlogscale = True,
        ylogscale = True,
        use_ux = True,
        xlabel = '$C$ [$\\si{\\farad}$]',
        ylabel = '$f$ [$\\si{\\hertz}$]',
        figfile = '2f.pdf',
    )

#stampo la tabella dei dati
tab3 = TabularContent()
tab3.add_column(Cin * 1e9) #in nanoFarad
tab3.add_column(ff / 1e3)  #in kiloHertz
tab3.save('tabella2f.tmp.tex')

# cose finali
latex_file.save('variabili.tmp.tex')
# Esegui lo script per simulare i circuiti
import simulate
