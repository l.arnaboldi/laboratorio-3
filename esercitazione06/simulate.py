
from numpy import *
from matplotlib import pyplot
from LabTools.plot import *

t, Vsh, Vd, Vout = genfromtxt('dati/circuiti_simulati/1.csv', delimiter=',', unpack = True)
t *= 1e3
multi_plot(
    data = [(t, Vout), (t, Vd), (t,Vsh)],
    xlabel = "t [\\si{\\milli \second}]",
    ylabel = "V [\\si{\\volt}]",
    figfile = "simulazione1.pdf",
)

t, Vout, Vp, Vm = genfromtxt('dati/circuiti_simulati/2.csv', delimiter=',', unpack = True)
t *= 1e6
multi_plot(
    data = [(t, Vout), (t,Vp), (t,Vm)],
    xlabel = "t [\\si{\\micro \\second}]",
    ylabel = "V [\\si{\\volt}]",
    figfile = "simulazione2.pdf",
)
