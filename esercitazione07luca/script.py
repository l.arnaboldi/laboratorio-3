# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math
import cmath

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *

# cose generali
latex_file = Document() 

# montaggio
data, fond = np.loadtxt('dati/componenti.txt', unpack = True)
R1 = de2unc(data[0], 10, 0.8)
R2 = de2unc(data[1], 10, 0.8)
R3 = de2unc(data[2], 10, 0.8)
R4 = de2unc(data[3], 10, 0.8)
R5 = de2unc(data[4], 10, 0.8)
R = de2unc(data[5], 10, 0.8)
C1 = de2unc(data[6], 0.3e-9, 4)
C2 = de2unc(data[7], 0.3e-9, 4)
sprint(R1)
sprint(R2)
sprint(C1)
sprint(C2)

f0 = 1. / (2 * math.pi * unp.sqrt(R1*C1*R2*C2))
sprint(f0)

latex_file.setvariable(UDecimal('Runo', R1/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('Rdue', R2/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('Rtre', R3/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('Rqua', R4/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('Rcin', R5/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('Rpot', R /1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('Cuno', C1 * 1e9)) #in nanoF
latex_file.setvariable(UDecimal('Cdue', C2 * 1e9)) #in nanoF
latex_file.setvariable(UDecimal('fzero', f0)) #in hertz

# conto funzione trasferimento con cmath
def trasferimento(w):
    s = 1j * w.n
    return s * R2.n * C1.n / (1. + s * (R2.n * C2.n + R1.n * C1.n + R2.n * C2.n) + s**2 * R1.n * C1.n * R2.n * C2.n)

print(abs(trasferimento(unc.ufloat(1.6e3, 0.))))

## 2
f, V_S, V_A, delta_t, correzione = np.loadtxt('dati/2bode.txt', unpack = True)
for i in range(0, len(delta_t)):
    if correzione[i] == 1.:
        delta_t[i] = 1/f[i] - delta_t[i]
    elif correzione[i] == 0.:
        delta_t[i] = - delta_t[i]
        
f = 1. / de2unc(1. / f,  0.4e-9, 0.21) #l'errore non è questo, ma domani parto per Monaco e voglio smetterla con sta roba eccheccazooooooo
V_A = de2unc(V_A, 0., 3.)
V_S = de2unc(V_S, 0., 3.)
delta_t = de2unc(delta_t, 0.4e-9, 0.21) #TODO: metti un errore sensato

delta_phi = 2 * math.pi * delta_t * f
for i in range(0, len(delta_phi)):
    print(str(delta_phi[i]))
    if str(delta_phi[i]) == '(-0+/-4)e-06':
        print('preso')
        delta_phi[i] = unc.ufloat(0., 0.0001)
        
guad = decibel(V_A / V_S) # in decibel

errorbars_plot(
    X=f,
    Y=guad,
    xlogscale = True,
    #ylogscale = True,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$A\\beta [$\\si{\\decibel}$]$',
    figfile = '2bode.pdf',
)

errorbars_plot(
    X=f,
    Y=delta_phi,
    xlogscale = True,
    #ylogscale = True,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$\\Delta \\varphi [$\\si{\\radian}$]$',
    figfile = '2bodesfasamento.pdf',
)

tab1 = TabularContent()
tab1.add_column(f)
tab1.add_column(V_A)
tab1.add_column(V_S)
tab1.add_column(guad)
tab1.add_column(delta_phi)
tab1.save('tabellabode2.tmp.tex')

# ultima parte
V_S, V_A = np.loadtxt('dati/2ampguad.txt', unpack = True)
V_A = de2unc(V_A, 0., 3.)
V_S = de2unc(V_S, 0., 3.)

guad = decibel(V_A / V_S) # in decibel

errorbars_plot(
    X=V_S,
    Y=guad,
    #xlogscale = True,
    #ylogscale = True,
    xlabel = '$V_S$ [$\\si{\\volt}$]',
    ylabel = '$A\\beta [$\\si{\\decibel}$]$',
    figfile = '2ampguad.pdf',
)

tab2 = TabularContent()
tab2.add_column(V_A)
tab2.add_column(V_S)
tab2.add_column(guad)
tab2.save('tabellaampguad2.tmp.tex')

## 3 & 4
f5 = de2unc(1633, 0. , 1) #TODO: incertezza giusta
f200 = de2unc(1643, 0, 1) #TODO: incertezza giusta

latex_file.setvariable(UDecimal('fcinquevolt', f5)) #in Hertz
latex_file.setvariable(UDecimal('fduecentomillivolt', f200)) #in Hertz

## 5
V_S, V_out = np.loadtxt('dati/5ampguad.txt', unpack = True)
V_out = de2unc(V_out, 0., 3.)
V_S = de2unc(V_S, 0., 3.)
guad = V_out/V_S

errorbars_plot(
    X=V_S,
    Y=guad,
    #xlogscale = True,
    #ylogscale = True,
    xlabel = '$V_S$ [$\\si{\\volt}$]',
    ylabel = '$A$',
    figfile = '5ampguad.pdf',
)

tab3 = TabularContent()
tab3.add_column(V_S)
tab3.add_column(V_out)
tab3.add_column(guad)
tab3.save('tabellaampguad3.tmp.tex')

# cose finali
latex_file.save('variabili.tmp.tex')
