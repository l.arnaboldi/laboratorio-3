
from fourier import *
from LabTools.plot import *
from numpy import *
from matplotlib import pyplot

import math

fT= 330. #hertz
fQ = 100. #hertz
guadagno = 10.

T = 1 / fQ
AMPLITUDE = 1.
SPACE_DIVISION = 10000
NTERM = 10000

fterm = range(0,NTERM+1)

L2 = L2Space([0., T])
fourier_sin = SinBase(L2)
xx = numpy.linspace(-2*L2.domain[1], 2*L2.domain[1], SPACE_DIVISION)
c_onda_quadra = numpy.array([2/(numpy.pi * n) if n % 2 == 1 else 0. for n in fterm]) * numpy.sqrt(2*T) * AMPLITUDE

def sfasamento(f):
    return arctan(-f/fT) + math.pi
    
def ampiezza(f):
    return guadagno / sqrt(1. + (f / fT)**2)
    
def onda_quadra(x):
	return fourier_sin.fcoefficients(c_onda_quadra , x)
    
def risposta(x):
    ans = 0.
    for i in fterm:
        #if i * fQ > 3e6:
        #    break
        ans += fourier_sin.e(i, x, sfasamento(i*fQ)) * c_onda_quadra[i] * ampiezza(i*fQ)
    return ans
    
pyplot.plot(xx, onda_quadra(xx))
pyplot.plot(xx, risposta(xx))
pyplot.show()
    
