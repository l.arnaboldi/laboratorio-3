# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *
from LabTools.measure import *


#cose generali
latex_file = Document()
tester = Tester('../multimetro_digitale.yaml')
osc = Oscilloscope('../oscilloscopio.yaml')

print('________PARTE 2________')
data, fond = np.loadtxt('dati/2d.txt', unpack = True)
tPLH = osc.time(data[0], fond = 10e-9*fond[0])
latex_file.setvariable(UDecimal('tPLHdue', tPLH*1.e9)) #in ns
tPHL = osc.time(data[1], fond = 10e-9*fond[1])
latex_file.setvariable(UDecimal('tPHLdue', tPHL*1.e9)) #in ns
sprint(tPLH)
sprint(tPHL)

vin, vout = np.loadtxt('dati/2d-volt.txt', )



print('________PARTE 3________')
# Misuro le resistenze
R = np.loadtxt('dati/3a.txt', unpack = True)
R1 = tester.resistance(R[0])
latex_file.setvariable(UDecimal('Runotre', R1)) #in Ohm
R2 = tester.resistance(R[1])
latex_file.setvariable(UDecimal('Rduetre', R2)) #in Ohm
R3 = tester.resistance(R[2])
latex_file.setvariable(UDecimal('Rtretre', R3)) #in Ohm
R4 = tester.resistance(R[3])
latex_file.setvariable(UDecimal('Rquattrotre', R4)) #in Ohm
# 3c
Q, VL, VH, tPHL, tPLH, fondHL, fondLH = np.loadtxt('dati/3c.txt', unpack = True)
VL = osc.voltage(VL)
VH = osc.voltage(VH)
tPHL = osc.time(tPHL, fondHL*10.)
tPLH = osc.time(tPLH, fondLH*10.)

tab = TabularContent()
tab.add_column(Q, show_unc = False, value_digits = 1)
tab.add_column(VL)
tab.add_column(VH)
tab.add_column(tPLH)
tab.add_column(tPHL)
tab.save('tabella3c.tmp.tex')

f_clock, Q, f_Q, fond = np.loadtxt('dati/3c-freq.txt', unpack = True)
f_clock = osc.frequency(f_clock, fond = fond*10)
f_Q = osc.frequency(f_Q, fond = fond*10)

f_Q_prev = numpy.zeros_like(f_clock)
for i in range(0, len(f_clock)):
    f_Q_prev[i] = f_clock[i] / 2**(i+1)


tab = TabularContent()
tab.add_column(Q, show_unc = False, value_digits = 1)
tab.add_column(f_clock)
tab.add_column(f_Q_prev)
tab.add_column(f_Q)
tab.save('tabella3c2.tmp.tex')


f_clock = osc.frequency(7.290e3)
f_clear = osc.frequency(728.5)

latex_file.setvariable(UDecimal('fclocktre', f_clock)) #in Hz
latex_file.setvariable(UDecimal('fclockdecimitre', f_clock/10.)) #in Hz
latex_file.setvariable(UDecimal('fcleartre', f_clear)) #in Hz





print('________PARTE 4________')
#4d
#Leggo i dati
Q, tU, fsU, tD, fsD = np.loadtxt('dati/4d.txt', unpack = True)
sprint(fsU)
print(fsU*1.e-5)
tU = osc.time_frequency(tU, fsU*1.e-5)
tD = osc.time_frequency(tD, fsD*1.e-5)

sprint(tU)
print(tU * 1.e6)

tab4d = TabularContent()
tab4d.add_column(Q, show_unc = False, value_digits = 1)
tab4d.add_column(tU * 1.e6)
tab4d.add_column(tD * 1.e6)
tab4d.add_column(100* tU / (tU + tD))
tab4d.save('tabella4d.tmp.tex')

#sprint(tD)

# cose finali
latex_file.save('variabili.tmp.tex')
