# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *


#cose generali
latex_file = Document()

#Componenti
print('________COMPONENTI________')
data, fond = np.loadtxt('dati/componenti.txt', unpack = True)
POT = de2unc(data[0], 10, 0.8) #resistenza totale del potenziometro
R1 = de2unc(data[1], 10, 0.8)
R2 = de2unc(data[2], 10, 0.8)
R3 = de2unc(data[3], 10, 0.8)
R4 = de2unc(data[4], 10, 0.8)
R5 = de2unc(data[5], 10, 0.8)
C1 = de2unc(data[6], 3e-10, 4.0)
C2 = de2unc(data[7], 3e-10, 4.0)
latex_file.setvariable(UDecimal('Runo', R1/1000.)) #in kOhm
latex_file.setvariable(UDecimal('Rdue', R2/1000.)) #in kOhm
latex_file.setvariable(UDecimal('Rtre', R3/1000.)) #in kOhm
latex_file.setvariable(UDecimal('Rquattro', R4/1000.)) #in kOhm
latex_file.setvariable(UDecimal('Rcinque', R5/1000.)) #in kOhm
latex_file.setvariable(UDecimal('Pot', POT/1000.)) #in kOhm
latex_file.setvariable(UDecimal('Cuno', C1*1.e9))  #in nFarad
latex_file.setvariable(UDecimal('Cdue', C2*1.e9))    #in nFarad
sprint(R1)
sprint(R2)
sprint(R3)
sprint(R4)
sprint(R5)
sprint(C1)
sprint(C2)
print()


#Punto2
print('________PUNTO 2________')

f, divf, vs, va, dt, divt = np.loadtxt('dati/bode2.txt', unpack = True)

T = de2unc(1. / f, divf / 250. + 0.4e-9, 0.01)
f = 1./T
Vs = de2unc(vs, 0., 3.0)
Va = de2unc(va, 0., 3.0)
dt = de2unc(dt, divt / 250. + 0.4e-9, 0.01)
A = decibel(Va / Vs)
Dphi = 2 * dt * f #in \pi-radianti

#stampo la tabella dei dati
tab1 = TabularContent()
tab1.add_column(f / 1e3)  #in kiloHertz
tab1.add_column(Vs * 1000.) #in milliVolt
tab1.add_column(Va * 1000.) #in milliVolt
tab1.add_column(A) #in Decibel
tab1.add_column(Dphi) #in \pi-rad
tab1.save('tabella1.tmp.tex')

#e li plotto
errorbars_plot(
    X=f,
    xlogscale = True,
    Y= A,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$\\beta A_V$ [$\\si{\\decibel}$]',
    figfile = 'bodeampiezza.pdf',
)

errorbars_plot(
    X=f,
    xlogscale = True,
    Y= Dphi,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$\\Delta\\varphi$ [$\\pi\\si{\\radian}$]',
    figfile = 'bodesfasamento.pdf',
)

#Faccio il conto della frequenza di Nyquist misurata e attesa
f0 = 1.614e3
T0 = de2unc(1./f0, 100e-6 / 250. + 0.4e-9, 0.01)
f0 = 1. / T0
sprint(f0)
latex_file.setvariable(UDecimal('fN', f0 / 1000.))    #in kiloHertz

f0att = 1./(2 * math.pi) * unp.sqrt(1. / (R1 * C1 * R2 * C2))
print(f0att)
latex_file.setvariable(UDecimal('fNatt', f0att / 1000.))    #in kiloHertz


#Leggo i dati della dipendenza di A da Vs e li plotto
vs2, va2 = np.loadtxt('dati/2potenziometro.txt', unpack = True)

Vs2 = de2unc(vs2, 0., 3.0)
Va2 = de2unc(va2, 0., 3.0)

A2 = decibel(Va2 / Vs2)

errorbars_plot(
    X=Vs2,
    Y= A2,
    xlabel = '$V_S$ [$\\si{\\volt}$]',
    ylabel = '$\\beta A_V$ [$\\si{\\decibel}$]',
    figfile = 'AVs.pdf',
)

#stampo la tabella dei dati
tab2 = TabularContent()
tab2.add_column(Vs2) #in Volt
tab2.add_column(Va2) #in Volt
tab2.add_column(A2) #in Decibel
tab2.save('tabella2.tmp.tex')

print()

#PUNTO 4
print('__________Punto 3 & 4__________')
#Leggo i dati

f, div = np.loadtxt('dati/punto4.txt', unpack = True)
T = de2unc(1. / f, div / 250. + 0.4e-9, 0.01)
f = 1./T

#Stampo la tabella dei dati
tab3 = TabularContent()
tab3.add_column(f / 1000.) #in kHz
tab3.save('tabella3.tmp.tex')

print()

#PUNTO 5
print('__________Punto 5__________')
#Leggo i dati
f, div = np.loadtxt('dati/punto5.txt', unpack = True)
T = de2unc(1. / f, div / 250. + 0.4e-9, 0.01)
f = 1./T

vs5, vout5 = np.loadtxt('dati/5.txt', unpack = True)

Vs5 = de2unc(vs5, 0., 3.0)
Vout5 = de2unc(vout5, 0., 3.0)

A5 = decibel(Vout5 / Vs5)

#stampo la tabella dei dati
tab5 = TabularContent()
tab5.add_column(Vs5) #in Volt
tab5.add_column(Vout5) #in Volt
tab5.add_column(A5) #in Decibel
tab5.save('tabella5.tmp.tex')

#eseguo il fit

#Primo fit
def guad1(x, k):
    return k * x

def Dguad1(x, k):
    return k

best51 = iterated_fit(guad1, Vs5, Vout5, df = Dguad1)
chi51, ndof51 = chi2iterated(guad1, best51, Vs5, Vout5, df = Dguad1)

Aatt1, = best51

sprint(Aatt1)
sprint(chi51)
sprint(ndof51)

latex_file.setvariable(UDecimal('Aattuno', Aatt1))
latex_file.setvariable(Decimal('chisquareduno', chi51))
latex_file.setvariable(Decimal('ndofuno', ndof51, digits = 1))

#Disegno il grafico con i parametri di best fit

residual_plot(
        f= guad1,
        df = Dguad1,
        param = best51,
        #outliers = [False] * (len(Cin) - 1) + [True],
        X=Vs5,
        Y=Vout5,
        use_ux = True,
        xlabel = '$V_s$ [$\\si{\\volt}$]',
        ylabel = '$V_{out}$ [$\\si{\\volt}$]',
        figfile = '51.pdf',
    )
   
#Secondo fit
def guad2(x, k, v0):
    return k * x + v0

def Dguad2(x, k, v0):
    return k

best52 = iterated_fit(guad2, Vs5, Vout5, df = Dguad2)
chi52, ndof52 = chi2iterated(guad2, best52, Vs5, Vout5, df = Dguad2)

Aatt2, v0= best52

sprint(Aatt2)
sprint(v0)
sprint(chi52)
sprint(ndof52)

latex_file.setvariable(UDecimal('Aattdue', Aatt2))
latex_file.setvariable(UDecimal('Voff', v0))
latex_file.setvariable(Decimal('chisquareddue', chi52))
latex_file.setvariable(Decimal('ndofdue', ndof52))

#Disegno il grafico con i parametri di best fit

residual_plot(
        f= guad2,
        df = Dguad2,
        param = best52,
        #outliers = [False] * (len(Cin) - 1) + [True],
        X=Vs5,
        Y=Vout5,
        use_ux = True,
        xlabel = '$V_s$ [$\\si{\\volt}$]',
        ylabel = '$V_{out}$ [$\\si{\\volt}$]',
        figfile = '52.pdf',
    )

A5 = Vout5 / Vs5

#Disegno il grafico V_{in} vs A
errorbars_plot(
    X=Vs5,
    Y= A5,
    xlabel = '$V_s$ [$\\si{\\volt}$]',
    ylabel = '$V_{out}$ [$\\si{\\volt}$]',
    figfile = '5guad.pdf',
)

print ()


# cose finali
latex_file.save('variabili.tmp.tex')

