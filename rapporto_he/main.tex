\documentclass[10pt,a4paper]{article}

\usepackage{relazione}


\include{variabili.tmp}

\captionsetup{width=1.2\linewidth}

%Numerazione subsection con lettere
\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\author{
	Gruppo B28 \\
	Luca Arnaboldi\thanks{luca@arnaboldi.lu},
	Francesco Loparco\thanks{francesco.loparco@live.it},
	Veronica Sacchi\thanks{ver22albireo@gmail.com}
}


\title{Misura rapporto $\frac{h}{e}$ \\ \texttt{versione telematica}}

\begin{document}
	\maketitle
	
	\section{Scopo dell'esperienza ed apparato strumentale}
	Lo scopo di quest'esperienza \`e la verifica dell'effetto fotoelettrico, attraverso l'osservazione di un andamento lineare che leghi frequenza ed energia, da cui ricavare una misura del rapporto $\frac{h}{e}$ attraverso la stima del relativo coefficiente angolare. 
	
	Data la modalit\`a telematica dell'esperienza il materiale a disposizione consiste essenzialmente di $4$ tabelle che riassumono l'intensit\`a della fotocorrente per diversi valori del potenziale di frenamento in presenza di $4$ diversi filtri, centrati su $4$ diverse lunghezze d'onda. 
	
	\section{Analisi dei dati}
	
	Per prima cosa \`e necessario determinare per ogni lunghezza d'onda il valore della tensione di frenamento $V_0$ per cui la fotocorrente si annulla; a tale fine si \`e deciso di adottare il secondo metodo di analisi proposto nel file \texttt{fotoelettrico-telematica.tex}, ampiamente discusso nella sottosezione successiva.
	
	
	\subsection*{Metodo di fit per determinare $V_0$}
	La funzione modello è 
	\begin{equation}
	f_\lambda(V) = a_\lambda \theta(V_{0,\lambda} - V)(V_{0,\lambda} - V)^\alpha + bV + I_0,
	\label{eq:fit-function-V0}
	\end{equation}
	con $\theta(\cdot)$ la funzione di Heaviside. La dipendenza della funzione da $\lambda$ deriva dai parametri $a$ e $V_0$; i parametri $\alpha$, $b$ e $I_0$ invece non dipendono dalla frequenza della radiazione incidente. Si è dunque effettuato un fit dei minimi quadrati, impostando una \emph{routine} che  minimizzi la funzione:
	\begin{equation}
	r(a_i, V_{0,i}, \alpha, b, I_0) = \sum_{\lambda} \sum_{j=1}^{N_\lambda} 
	\frac{\left(I_{\lambda,j} - f(V_{\lambda,j}; a_\lambda, V_{0,\lambda}, \alpha, b, I_0)\right)^2}{\sigma_{I_\lambda,j}^2 + \left(\frac{df}{dV}\sigma_{V_\lambda,j}\right)^2}
	\label{eq:residui}
	\end{equation}
	dove con $N_\lambda$ si intende il numero di dati nel set acquisito con una radiazione incidente di lunghezza d'onda $\lambda$. Al denominatore compare la derivata di $f$ perché si sta tentando di tenere in considerazione l'errore su $V$, propagandolo al prim'ordine su quello in $I$ e sommandolo in quadratura. Il fit ha dunque come parametri i valori di $a_\lambda$ e $V_{0,\lambda}$ per ogni lunghezza d'onda, mentre $\alpha$, $b$ e $I_0$ sono gli stessi al variare di $\lambda$; dato che in questo caso avevamo 4 lunghezze d'onda, in totale avremo $4+4+3=11$ parametri di fit.
	
	Si vuole eseguire un fit contemporaneamente su tutti e $4$ i data set, vincolando i tre parametri indipendenti da $\lambda$ a essere comuni, ma data la funzione da minimizzare non convenzionale e l'elevato numero di parametri, la \emph{routine} ha difficoltà a convergere correttamente. Per ovviare a questo problema sono stati stimati preliminarmente dei limiti sui parametri iniziali da fornire alla funzione di minimizzazione, e si \`e effettuato un fit per ogni set di dati, in maniera indipendente. Successivamente i risultati preliminari ottenuti sono stati forniti alla funzione  \texttt{scipy.optimize.least\_squares} come parametri iniziali del fit completo.
	
	L'incertezza sui parametri è stata stimata utilizzando la relazione tra matrice di covarianza $\sigma$ ed hessiana valutata nel minimo $H_r$ della funzione da minimizzare:
	\[\sigma =  \left(H_r\right)^{-1}.\]
	Come fornito in documentazione della funzione \texttt{scipy.optimize.least\_squares} un'approssimazione della matrice hessiana si può trovare come 
	\[H_r = J^T J,\]
	dove $J$ è lo \emph{Jacobiano modificato} della funzione dei residui. Per maggiori dettagli si rimanda alla documentazione, consultabile \url{https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.least_squares.html}.
	
	In questo modo \`e stato possibile ottenere i grafici in figura \ref{fig:fitV0}, in cui \`e stata rappresentata anche la curva corrispondente ai valori di \emph{best-fit} riportati in tabella \ref{tab:risultati-fit-V0}.
	
	\begin{table}[ht!]
		\centering
		\begin{tabular}{c|c|c|c}
			$\lambda[\si{\nano\meter}]$ & $\nu[\si{\tera\hertz}]$ & $V_0[\si{\volt}]$& $a$ \\
			\hline \hline
			\input{multi-fit.tmp.tex}
		\end{tabular}
		\caption{parametri di best-fit ottenuti minimizzando la funzione \eqref{eq:residui} in corrispondenza di diverse lunghezze d'onda, o equivalentemente diverse frequenze, legate alla lunghezza d'onda dalla relazione $\lambda\nu = c$, dove $c$ \`e la velocit\`a della luce.}
		\label{tab:risultati-fit-V0}
	\end{table}
	
	Gli altri parametri di best-fit, comuni a tutte le lunghezze d'onda sono:
	\begin{align*}
		\alpha &= \num{\alphafit}\\
		b &= \SI{\bfit}{\tera\ohm}^{-1}\\
		I_0 &= \SI{\Izero}{\pico\ampere}\\
		r(a_i, V_{0,i}, \alpha, b, I_0)_{best-fit} &= \num{\cost}\\
		\text{ndof} &= \num{\ndof}
	\end{align*}
	
	\begin{figure}
		\centering
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\caption{punti sperimentali, con relative barre d'errore, relativi a una radiazione incidente di lunghezza d'onda $\lambda \sim \SI{450}{\nano\meter}$, sovrapposti alla curva di best-fit, indicata in blu, ottenuta attraverso la \emph{routine} descritta precedentemente.}
			\includegraphics[width = \textwidth]{450.pdf}
			\vspace*{0.3cm}
			\label{fig:450}
		\end{subfigure}
		\hfill
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\caption{punti sperimentali, con relative barre d'errore, relativi a una radiazione incidente di lunghezza d'onda $\lambda \sim \SI{499}{\nano\meter}$, sovrapposti alla curva di best-fit, indicata in blu, ottenuta attraverso la \emph{routine} descritta precedentemente.}
			\includegraphics[width = \textwidth]{499.pdf}
			\vspace*{0.3cm}
			\label{fig:499}
		\end{subfigure}
		\begin{subfigure}[b]{0.49\textwidth}
			\caption{punti sperimentali, con relative barre d'errore, relativi a una radiazione incidente di lunghezza d'onda $\lambda \sim \SI{546}{\nano\meter}$, sovrapposti alla curva di best-fit, indicata in blu, ottenuta attraverso la \emph{routine} descritta precedentemente.}
			\includegraphics[width = \textwidth]{546.pdf}
			\vspace*{0.3cm}
			\label{fig:546}
		\end{subfigure}
		\hfill
		\begin{subfigure}[b]{0.49\textwidth}
			\caption{punti sperimentali, con relative barre d'errore, relativi a una radiazione incidente di lunghezza d'onda $\lambda \sim \SI{577}{\nano\meter}$, sovrapposti alla curva di best-fit, indicata in blu, ottenuta attraverso la \emph{routine} descritta precedentemente.}
			\includegraphics[width = \textwidth]{577.pdf}
			\vspace*{0.3cm}
			\label{fig:577}
		\end{subfigure}
		\caption{grafici e relativo plot dei residui degli andamenti dei punti sperimentali della fotocorrente al variare della tensione di frenamento, sovrapposti alle curva di best fit modellizzate dalla funzione \eqref{eq:fit-function-V0}.}
		\label{fig:fitV0}
	\end{figure}
	
	\subsection{Metodo di fit per determinare il rapporto $\frac{hc}{e}$}
	A questo punto \`e possibile eseguire un ultimo fit per determinare il rapporto $\frac{hc}{e}$ a partire dalla relazione:
	\begin{equation}
	V_0 = \frac{hc}{e}\frac{1}{\lambda} - \frac{W_0}{e}.
	\label{eq:fit-function-h/e}
	\end{equation}
	ed \`e sufficiente utilizzare la funzione \texttt{curve\_fit} all'interno di un \emph{fit iterato} (in modo da tenere conto dell'incertezza sulle lunghezze d'onda); l'incertezza associata a $\lambda$ non \`e trascurabile e deriva dal fatto che il filtro utilizzato ha una banda passante larga circa $\SI{10}{\nano\meter}$: assumendo errori gaussiani quindi sembra opportuno prendere come valor medio della lunghezza d'onda il valore nominale del filtro, e come incertezza associata sempre $\delta\lambda = \SI{2}{\nano\meter}$, perch\`e la funzione di partizione della gaussiana raggiunge il $99\%$ per una banda di semilarghezza $3\sigma$ centrata attorno al valor medio.\\
	I risultati di questo ultimo fit lineare sono
	\begin{align}
		\frac{hc}{e} &=\SI{\hce}{\volt \cdot \meter}  \label{eq:risultati-finali}\\
		\frac{W_0}{e} &= \SI{\wzeroe}{\volt}\\
		\chi ^2/\text{ndof} &= \chiquadro/\ndofinal
	\end{align}
	
	e per maggiore chiarezza sono stati riportati i punti sperimentali sovrapposti alla curva di best fit in figura \ref{fig:fit-finale}. Nell'ultima sezione della relazione \`e stato riportato il codice \texttt{Python} utilizzato per l'analisi.
	
	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{final-fit.pdf}
		\caption{Punti sperimentali con relative barre d'errore sovrapposti alla curva di best fit, con plot dei residui associato.}
		\label{fig:fit-finale}
	\end{figure}
	Infine osserviamo che la funzione modello \eqref{eq:fit-function-h/e} può essere riscritta come:
	\begin{equation}
	V_0 = \frac{h}{e}\nu - \frac{W_0}{e}.
	\label{eq:h/e}
	\end{equation}
	introducendo la frequenza $\nu = \frac{c}{\lambda}$; la \eqref{eq:h/e} \`e semplicemente una retta, e disegnando l'andamento corrispondente ai parametri di best fit individuati nella \eqref{eq:risultati-finali} si ottiene il grafico \ref{fig:fit-finale-retta}, dove si evidenzia il buon accordo dei punti sperimentali con l'andamento lineare che ci si era preposti di verificare.
	
	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{final-fit-retta.pdf}
		\caption{Punti sperimentali con relative barre d'errore sovrapposti alla retta di best fit, con plot dei residui associato.}
		\label{fig:fit-finale-retta}
	\end{figure}
	
	\newpage
	\section{Commenti finali}
	
	Il valore tabulato del rapporto $\frac{hc}{e}$ vale\footnote{effettuando il rapporto tra le due costanti fondamentali il cui valore \emph{esatto} \`e tabulato dal NIST in \url{https://physics.nist.gov/cgi-bin/cuu/Value?h} ($h$) e \url{https://physics.nist.gov/cgi-bin/cuu/Value?e} ($e$), moltiplicate per la velocit\`a della luce, anch'esso valore riportato \emph{esattamente} in \url{https://physics.nist.gov/cgi-bin/cuu/Value?c}.}
	\begin{equation}
	\frac{hc}{e}=\SI{1.23984198406e-6}{\volt\cdot\meter},
	\label{eq:val-tabulato}
	\end{equation}
	
	compatibile con il valore in \eqref{eq:risultati-finali}, da noi stimato. \\
	Osserviamo che i grafici di figura \ref{fig:fitV0} presentano un plot dei residui con un andamento molto ordinato per tensioni inferiori a $V_0$: si sospetta che siano un effetto sistematico che manifesta alcuni fenomeni non inclusi nel nostro modello, possibilmente anche legati all'interazione con l'apparato stumentale di misura, che tuttavia non incide significativamente sulla nostra analisi in quanto, come si \`e visto, il valore di $\frac{hc}{e}$ rimane compatibile con quello tabulato.
	
	%	\begin{itemize}
	%		\item Nonostante le figure \ref{eq:fit-function-V0} riportino, nella sezione dei residui, un andamento periodico degli stessi, per cui si sospetta esserci un errore sistematico di cui, purtroppo, non possiamo occuparci perché non sono state effettuate da noi quelle misure, il valore finale del rapporto $\frac{h}{e}$ risulta compatibile con quello fornito dal NIST.  
	%		
	%		
	%		
	%		\item La figura \ref{fig:fit-finale} è quella dove si espone l'accordo tra i punti sperimentali e il modello selezionato per studiare l'effetto fotoelettrico. Da questo fit abbiamo ottenuto il nostro valore della costante $\frac{h}{e}$: \\
	%		il quale risulta, compatibile.
	%	\end{itemize}
	
	\section*{Dichiarazione}
	I firmatari di questa relazione dichiarano che il contenuto della relazione \`e originale, 
	e che tutti i firmatari hanno contribuito alla elaborazione della relazione stessa.
	\newpage
	
	\section*{Codice Python per  l'analisi}
	Dato che è stata utilizzata una funzione di fit non standard, viene riportato il codice \texttt{Python} utlizzato per l'analisi.
	\inputminted[xleftmargin=\parindent,linenos]{Python}{script.py}
	
	
\end{document}


