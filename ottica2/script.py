import numpy as np
from uncertainties import unumpy as unp
import uncertainties as uncert
import uncertainties.umath as umath

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *

latex_file = Document()
d = uncert.ufloat(1e-3,0.)

def model(x, a, b):
    return a*x+b

def elaborate(m, h, D, tag):
    h_, uh = unpack_unarray(h)
    stima_errore = D.s/D.n/np.sqrt(sum((uh/h_)**2/len(h)))
    sprint(stima_errore)
    latex_file.setvariable(Decimal("stimaerrore"+tag, stima_errore))

    latex_file.setvariable(UDecimal("D"+tag, D))
    theta = np.pi/2.-unp.arctan(h/D)
    if m[0] == 0.:
        latex_file.setvariable(UDecimal("thetazero"+tag, theta[0]))
    Y = unp.sin(theta)
    # for y in Y:
    #     print("")
    #     for (var, error) in y.error_components().items():
    #         print("{}: {}".format(var.tag, error))

    tab = TabularContent()
    tab.add_column(h)
    tab.add_column(theta)
    tab.add_column(Y)
    tab.save("tab1"+tag+".tmp.tex")

    A, B = ucurve_fit(model, m, Y, absolute_sigma = False)
    chi, ndof = chi2(model, (A,B), m, Y)
    latex_file.setvariable(Decimal("chi"+tag, chi))
    latex_file.setvariable(Decimal("ndof"+tag, ndof))
    lambda_laser = -A*d
    theta_i  = umath.asin(B)
    latex_file.setvariable(UDecimal("lambdalaser"+tag, lambda_laser*1e9)) # in nanometri
    latex_file.setvariable(UDecimal("thetai"+tag, theta_i))


    sprint(lambda_laser)
    sprint(theta_i)
    sprint(chi)
    sprint(ndof)

    residual_plot(
        f = model,
        param = (A,B),
        X = m,
        Y = Y,
        outliers = None,
        xlogscale = False,
        use_ux = False,
        xlabel = 'm',
        ylabel = '$\\sin{\\theta_d}$',
        figfile = "grafico"+tag+".pdf",
    )

#A04
print("______A04______")
D_, uD = np.loadtxt("dati/A04calib.txt", unpack=True)
D = uncert.ufloat(D_,uD)
m, h1, h2 = np.loadtxt("dati/A04.txt", unpack=True)
m = de2unc(m, 0.)
uh = (h2-h1)/2.
h_ = (h1+h2)/2.
h = de2unc(h_, uh) #in cm
elaborate(m, h, D, 'Aquattro')

# uD = uD / 2.
# uh = uh / 2.
# D = uncert.ufloat(D_,uD)
# h = de2unc(h_, uh)
# elaborate(m, h,  D, 'Aquattrodue')
#
# uD = 2. * uD / 3.
# uh = 2. * uh / 3.
# D = uncert.ufloat(D_,uD)
# h = de2unc(h_, uh)
# elaborate(m, h,  D, 'Aquattrotre')

#A06
print("______A06______")
D_, uD = np.loadtxt("dati/A06calib.txt", unpack=True)
D = uncert.ufloat(D_,uD)
m, h_, uh = np.loadtxt("dati/A06.txt", unpack=True)
m = de2unc(m, 0.)
h = de2unc(h_, uh) #in cm
elaborate(m, h, D, 'Asei')

# uD = uD / 2.
# D = uncert.ufloat(D_,uD)
# elaborate(m, h,  D, 'Aseidue')
#
# uD = 2. * uD / 3.
# D = uncert.ufloat(D_,uD)
# elaborate(m, h,  D, 'Aseitre')

#B01
print("______B01______")
D_, uD = np.loadtxt("dati/B01calib.txt", unpack=True)
D = uncert.ufloat(D_,uD)
m, h_, uh = np.loadtxt("dati/B01.txt", unpack=True)
m = de2unc(m, 0.)
h = de2unc(h_, uh) #in cm
elaborate(m, h, D, 'Buno')

# uD = uD / 2.
# uh = uh / 2.
# D = uncert.ufloat(D_,uD)
# h = de2unc(h_, uh)
# elaborate(m, h,  D, 'Bunodue')
#
# uD = 2. * uD / 3.
# uh = 2. * uh / 3.
# D = uncert.ufloat(D_,uD)
# h = de2unc(h_, uh)
# elaborate(m, h,  D, 'Bunotre')

#B09
print("______B09______")
D_, uD = np.loadtxt("dati/B09calib.txt", unpack=True)
D = uncert.ufloat(D_,uD)
m, h = np.loadtxt("dati/B09.txt", unpack=True)
m = de2unc(m, 0.)
h = de2unc(h, 0.1) #in cm
elaborate(m, h,  D, 'Bnove')

# uD = uD / 2.
# D = uncert.ufloat(D_,uD)
# elaborate(m, h,  D, 'Bnovedue')
#
# uD = 2. * uD / 3.
# D = uncert.ufloat(D_,uD)
# elaborate(m, h,  D, 'Bnovetre')

#self
print("______self______")
Dmin, Dmax = np.loadtxt("dati/self-calib.txt", unpack=True)
D = uncert.ufloat((Dmin+Dmax)/20.,(Dmax-Dmin)/60.)
m, hmin, hmax = np.loadtxt("dati/self.txt", unpack=True)
m = de2unc(m, 0.)
h = de2unc((hmin+hmax)/2., 0.1+(-hmin+hmax)/2.) #in cm
elaborate(m, h,  D, 'self')


# cose finali
latex_file.save('variabili.tmp.tex')
