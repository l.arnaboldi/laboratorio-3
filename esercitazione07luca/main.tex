\documentclass[10pt,a4paper]{article}

\usepackage{relazione}

\include{variabili.tmp}

\captionsetup{width=1.2\linewidth}

%Numerazione subsection con lettere
\renewcommand{\thesubsection}{\thesection.\alph{subsection}}


\title{Esercitazione 07: Oscillatore sinusoidale a ponte di Wien con OpAmp.}

\author{Luca Arnaboldi\thanks{luca.arnaboldi@arn4.it}}

\begin{document}
\maketitle

\section{Scopo e strumentazione}
Lo scopo dell'esperienza è realizzare un \emph{oscillatore ad onda
sinusoidale a ponte di Wien} secondo lo schema mostrato nella
Figura~\ref{fig:circuitofeedback}.
\begin{figure}
	\centering
	\caption{schemi circuitali dell'esperienza.}
	\label{fig:circuiti}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{circuitosenzafeedback.pdf}
		\caption{schema circuitale dell'oscillotaore di Wien, senza feedback.}
		\label{fig:circuitosenzafeedback}
	\end{subfigure}
	\begin{subfigure}{0.5\textwidth}
		\includegraphics[width=\textwidth]{circuitofeedback.pdf}
		\caption{schema circuitale dell'oscillotaore di Wien, con feedback.}
		\label{fig:circuitofeedback}
	\end{subfigure}
\end{figure}

La strumentazione utilizzata consiste di OpAmp, condensatori,
resistitori, trimmer e i generatori di onde e di tensione come
componenti, oscilloscopio e multimetro digitale per le misure.

I valori dei componenti circuitali scelti sono:
\begin{align*}
	&C_1 = \SI{\Cuno}{\nano\farad}\quad
	C_2 = \SI{\Cdue}{\nano\farad},\\
	&R_1 = \SI{\Runo}{\kilo\ohm}\quad
	R_2 = \SI{\Rdue}{\kilo\ohm}\quad
	R_3 = \SI{\Rtre}{\kilo\ohm},\\
	&R_4 = \SI{\Rqua}{\kilo\ohm}\quad
	R_5 = \SI{\Rcin}{\kilo\ohm}.
\end{align*}

\newpage
\section{Analisi del \emph{loop-gain}}
%TODO: come ho fatto effettivamente le misure
Per questa sezione si è usato il circuito mostrato nella configurazione
Figura~\ref{fig:circuitosenzafeedback}, con il generatore d'onde impostato ad ampiezza $\sim \SI{250}{\milli\volt}$. Si è quindi variata la frequenza
dell'onda in ingresso, misurando guadagno e sfasamento. I dati raccolti sono
riportati in Tabella~\ref{tab:bodedue}.
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|}
			\hline
			$f [\si{\hertz}]$ &
			$V_A [\si{\volt\text{pp}}]$ &
			$V_S [\si{\volt\text{pp}}]$ &
			$A\beta [\si{\decibel}]$ &
			$\Delta \varphi [\si{\pi \radian}]$ \\
			\hline
			\input{tabellabode2.tmp.tex}
			\hline
		\end{tabular}
		\caption{guadagno e sfasamento del circuito. }
		\label{tab:bodedue}
	\end{center}
\end{table}
I dati sono anche riportati in diagramma di Bode, in Figura~\ref{fig:bodedue}.
\begin{figure}
	\centering
	\caption{grafici di Bode per il circuito in modalità \emph{loop-gain}.}
	\label{fig:bodedue}
	\begin{subfigure}{0.94\textwidth}
		\caption{ampiezza}
		\includegraphics[width=\textwidth]{2bode.pdf}
		\label{fig:bodedueamp}
	\end{subfigure}
	\begin{subfigure}{0.94\textwidth}
		\caption{sfasamento}
		\includegraphics[width=\textwidth]{2bodesfasamento.pdf}
		\label{fig:bodeduesfa}
	\end{subfigure}
\end{figure}

La frequenza alla quale teoricamente lo sfasamento dovrebbe essere nullo è la stessa
frequenza alla quale il guadagno è massimo, che vale teoricamente
\[
f_\text{0,teo} = \frac{1}{\sqrt{R_1 C_1 R_2 C_2}} = \SI{\fzero}{\hertz}.
\]
La frequenza alla quale lo sfasamento si annulla è stata valutata in due modi differenti:
\begin{itemize}
	\item \emph{confronto dei due segnali in uscita}: si è variata la frequenza fino a
	quando sull'oscilloscopio non era più possibile misurare lo sfasamento attarverso i
	due cursori. Facendo la semidispersione tra la frequenza più bassa e quella più alta
	per cui avveniva la cosa si è trovato 
	\[
		f_\text{0,sin} = \SI{1642 \pm 12}{\hertz};
	\]
	\item \emph{degenerazione del diagramma \texttt{XY}}: si è selezionata la
	modalità di visualizzazione dell'oscilliscopio in modo che i due assi fossero
	rispettivamente $V_S$ e $V_A$. Dato che sono entrambe onde sinusoidali alla
	stessa frequenza il risultato atteso a schermo è un elisse, che degenera in un
	segmento quando le due onde in ingresso sono in fase. Valutando la frequenza
	minima e massima per cui non era più possibile distinguere l'ellisse da un
	segmento si è trovato
	\[
	f_\text{0,XY} = \SI{1646 \pm 7}{\hertz}.
	\]
	In Figura~\ref{fig:modalitaXY} sono riportate delle acquisizione dello schermo dell'oscilliscopio
	nella modalità \texttt{XY}.
	\begin{figure}
		\centering
		\caption{acquisizioni dello schermo dell'oscilloscopio in modalità \texttt{XY}
	per due diverse frequenze.}
		\label{fig:modalitaXY}
		\begin{subfigure}[b]{0.45\textwidth}
			\caption{frequenza diversa da quella a sfasamento nullo}
			\includegraphics[width=\textwidth]{dati/nyquivistacaso.png}
			\label{fig:modalitaXYesempio}
		\end{subfigure}
		~
		\begin{subfigure}[b]{0.45\textwidth}
			\caption{frequenza a sfasamento nullo}
			\includegraphics[width=\textwidth]{dati/nyquivistf0.png}
			\label{fig:modalitaXYeffettivo}
		\end{subfigure}
	\end{figure}
\end{itemize}
Osserviamo come entrambi i valori sono compatibili con quanto atteso
teoricamente, oltre che esserlo tra loro.

Scegliendo ora una frequenza il più possibile vicina a $f_0$ si è variata la
posizione del potenziometro per osservare come cambiava il segnale all'uscita.
Le acquisizioni dello schermo sono riporatate in
Figura~\ref{fig:ampiezzapotenziometro}. Qualitativamente si vede che il guadagno
cresce al decrescere della posizione del potenziometro; in particolare vale
$\sim1$ quando il potenziometro è circa ai un terzo del suo valore massimo.
\begin{figure}
	\centering
	\caption{acquisizioni dello schermo dell'oscilloscopio per segnali in ingresso
	a frequenza $f_0$, al variare della posizione del potenziometro. Osserviamo che
	al decrescere della posizione del potenziometro anche il guadagno del circuito
	cresce.}
	\label{fig:ampiezzapotenziometro}
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{potenziometro $\sim 1$}
		\includegraphics[width=\textwidth]{dati/2pot0.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{potenziometro $\sim \frac{2}{3}$}
		\includegraphics[width=\textwidth]{dati/2pot13.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\caption{potenziometro $\sim \frac{1}{3}$}
		\includegraphics[width=\textwidth]{dati/2pot23.png}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\caption{potenziometro $\sim 0$}
		\includegraphics[width=\textwidth]{dati/2potintero.png}
	\end{subfigure}
\end{figure}

Infine, sempre con frequenza $f_0$ si è valutato il guadagno al variare
dell'ampiezza del segnale in ingresso. I dati raccolti sono riportati in
Tabella~\ref{tab:dueampguad} e in Figura~\ref{fig:dueampguad}. Effettivamente il
guadagno diminuisce all'aumentare dell'ampiezza, anche se l'effetto è ben più
visibile nei dati nella Sezione~\ref{sec:sezionecinque}, in Figura~\ref{fig:sezionecinque}.
\begin{table}
	\begin{center}
		\begin{tabular}{|c|c|c|}
			\hline
			$V_A [\si{\volt\text{pp}}]$ &
			$V_S [\si{\volt\text{pp}}]$ &
			$A\beta [\si{\decibel}]$ \\
			\hline
			\input{tabellaampguad2.tmp.tex}
			\hline
		\end{tabular}
		\caption{ampiezza del segnale in uscita al variare di $V_S$ in ingresso, per una
			frequenza $\sim f_0$.}
		\label{tab:dueampguad}
	\end{center}
\end{table}
\begin{figure}
	\centering
\caption{guadagno al variare di $V_S$ del circuito senza chiusura del ciclo di feedback. Notiamo
	che effettivamente il guadagno diminuisce all'aumentare di $V_S$, anche se i
	primi
	5 punti sono tutti compatibili. Per un analisi più accurata si sarebbero
	dovute esplorare
	voltaggi più alti. In ogni caso nella Sezione~\ref{sec:sezionecinque} si sono
	prese misure
	che riguardano lo stesso effetto, per ciò nel grafico in Figura~\ref{fig:sezionecinque} si
	nota che 
	effettivamente si vede l'andamento aspettato.}
\label{fig:dueampguad}
\includegraphics[width=0.9\textwidth]{2ampguad.pdf}	
\end{figure}

\newpage
\section*{3 \& 4 \quad Oscillatore con feedback}
Si è montato quindi il circuito nella configurazione della
Figura~\ref{fig:circuitofeedback}. Ruotando il potenziometro si nota che esiste
un valore soglia al di sotto del quale il segnale in uscita è piatto a
\SI{0}{\volt}, mentre al di sopra di tale valore si osserva un'onda sinusoidale
di frequenza
\[
f = \SI{\fcinquevolt}{\hertz}.
\]
Il valore che ci aspetterebbe dall'analisi teorica del circuito è $f_\text{0,teo}$. Il valore
qui misurato è quindi compatibile con quanto si prevede, ed è anche compatibile con le precedenti
misure di $f_0$.

La frequenza del segnale non è particolarmente influenzata dalla posizione del
potenziometro al di sopra della soglia. Si nota invece un'importante dipendenza
dell'ampiezza da questo parametro: al descrescere di $f$ del potenziometro
si ha una crescita dell'ampiezza, fino ad arrivare al clipping.
In Figura~\ref{fig:puntotrequattro} sono riportati diversi segnali al variare
della posizione del potenziometro.
Infine, anche se non visibile in figura, si nota che anche il tempo necessario
per arrivare a regime nell'oscillizazione dipende dalla posizione del potenziometro;
in paricolare si nota che più si è vicini alla soglia più tempo è necessario per
arrivare ad un'oscillazione ad ampiezza costante.
\begin{figure}
	\centering
	\caption{acquisizioni dello schermo dell'oscilloscopio mostrante il segnale
		$V_\text{out}$ al variare della posizione del potenziometro sopra la
		soglia. L'ampiezza teoricamente può essere ridotta a piacere fino a 0,
		anche se nella pratica ciò risulta impossibile dato dato che la regolazione
		del potenziometro non è tanto risoluta quanto necessario (questo
		effetto è anche accentuato dal fatto che la curva del guadagno è
		piatta per voltaggi bassi. Si veda la Sezione~\ref{sec:sezionecinque}
		per più dettagli); il segnale ad ampiezza più bassa che si è riusciti
		a misurare è a $\SI{800}{\milli\volt\textrm{pp}}$, come mostrato nella Figura~\ref{fig:puntotrequattroa}. \\
		Notiamo che l'ampiezza cresce al decrescere del parametro $f$ del potenziometro,
		e in particolare al di sotto di un certo valore inizia a vedersi il
		\emph{clipping}, come si può vedere il Figura~\ref{fig:puntotrequattroe}. Anche se non visibile nelle acquisizioni qui riportate, anche la durata del clipping dipende in maniera significativa dalla possizione del potenziometro.
	}
	\label{fig:puntotrequattro}
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{}
		\label{fig:puntotrequattroa}
		\includegraphics[width=\textwidth]{dati/3-0_800.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{}
		\includegraphics[width=\textwidth]{dati/3-6_8.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{}
		\includegraphics[width=\textwidth]{dati/3-10_4.png}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{}
		\includegraphics[width=\textwidth]{dati/3-15_6.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{}
		\label{fig:puntotrequattroe}
		\includegraphics[width=\textwidth]{dati/3-clipping.png}
		\vspace*{0.3cm}
	\end{subfigure}
	
	
\end{figure}

\newpage
\setcounter{section}{4} 
\section{Guadagno atteso}\label{sec:sezionecinque}
Si è posizionato il potenziometro nella posizione più vicina possibile all'innesco (quella che produce
il segnale in Figura~\ref{fig:puntotrequattroa}) e poi, senza muovere ulteriormente
il potenziometro, si è cambiato il circuito alla configurazione della
Figura~\ref{fig:circuitosenzafeedback}. Si è quindi misurato il guadagno in funzione
dell'ampiezza in ingresso; i dati sono riportati in Tabella~\ref{tab:datisezionecinque}
e rappresentati in Figura~\ref{fig:sezionecinque}.

I dati mostrano come il rapporto $V_\text{out}/V_S$ è compatibile con $3$
solamente per $V_S$ sufficientemente piccolo: oltre ad un certo valore infatti
il guadagno decresce, coerentemente con il modello teorico.
\begin{table}
	\begin{center}
		\begin{tabular}{|c|c|c|}
			\hline
			$V_S [\si{\volt\text{pp}}]$ &
			$V_\text{out} [\si{\volt\text{pp}}]$ &
			$A$ \\
			\hline
			\input{tabellaampguad3.tmp.tex}
			\hline
		\end{tabular}
		\caption{guadagno in uscita al variare di $V_S$ in ingresso. 
			La frequenza dell'onda in ingresso è~$\sim f_0$.}
		\label{tab:datisezionecinque}
	\end{center}
\end{table}
\begin{figure}
	\centering
	\caption{guadagno in uscita al variare di $V_S$ in ingresso. Vediamo che, come ci si aspetta dal modello teorico, a basse $V_S$ il guadagno è effettivamente compatibile con $3$, mentre al crescere di $V_S$ la non linearità dei diodi entra in gioco e il guadagno descresce.}
	\label{fig:sezionecinque}
	\includegraphics[width=0.9\textwidth]{5ampguad.pdf}	
\end{figure}

\newpage
\section{Funzione dei diodi}
Si è ripristinato il circuito come in Figura~\ref{fig:circuitofeedback}, ma
rimuovendo i due diodi. Le acquisizioni dello schermo dell'oscilloscopio
relative al segnale $V_\text{out}$ sono riportate in Figura~\ref{fig:senzadiodi}. Si nota che per le posizioni
del potenziometro per cui prima non c'era oscillazione anche ora non si vede nessun segnale oscillante. Dove prima invece c'era oscillazione anche ora si vede un segnale 
oscillante, ma a differenza di prima \emph{indipendentemente} dalla posizione del potenziometro si raggiunge clipping.

I diodi hanno quindi la funzione di rendere l'oscillazione stabile, introducendo una non linearità. Più nel dettaglio per segnali piccoli (precisamente al di sotto di $V_\text{th}$ dei diodi) sono in interdizione, quindi si comportano come circuiti aperti e la resistenza equivalente del loro parallelo con $R_3$ è proprio $R_3$. Viceversa per segnali grandi uno dei due diodi entra in conduzione e quindi si comporta come un cortocircuito, e la resistenza del parallelo è pressochè nulla. Nell'oscillatore quindi se l'ampiezza diventa troppo grande la resistenza $R_3$ viene esclusa, e quindi anche nell'ingresso $V_-$ dell'OpAmp il segnale diventa grande e ciò limita il processo per cui l'ampiezza cresce fino ad arrivare al clipping.

\begin{figure}
	\centering
	\caption{oscillatore a ponte di Wien senza diodi. L'oscillazione non è stabile e raggiunge il clipping inpendentemente dalla posizione del potenziometro. Con il potenziometro in posizione di soglia non si vede esplicitamente il clipping, come mostrarto in Figura~\ref{fig:hounsonnocheguarda}: questo è probabilmente dovuto al fatto che l'ampiezza del clipping dipende dalla posizione del potenziometro, e vicini alla soglia essa è talmente piccola da non essere rilevabile.}
	\label{fig:senzadiodi}
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{potenziometro $\sim 0$}
		\includegraphics[width=\textwidth]{dati/6potintero.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\hfill
	\begin{subfigure}[t]{0.49\textwidth}
		\caption{}
		\includegraphics[width=\textwidth]{dati/6pot23.png}
		\vspace*{0.3cm}
	\end{subfigure}
	\begin{subfigure}[b]{0.49\textwidth}
		\caption{}
		\includegraphics[width=\textwidth]{dati/6pot23(unpomenoinrealta).png}
	\end{subfigure}
	\hfill
	\begin{subfigure}[b]{0.49\textwidth}
		\caption{potenziometro alla soglia} \label{fig:hounsonnocheguarda}
		\includegraphics[width=\textwidth]{dati/6potapparentementesenzaclipping.png}
	\end{subfigure}
\end{figure}

\newpage
\section*{Dichiarazione}
Il firmatario di questa relazione dichiara che il contenuto della relazione 
\`e originale, con misure effettuate dal sottoscritto, e che solo il firmatario
ha contribuito alla elaborazione della relazione stessa. 
	
\end{document}
