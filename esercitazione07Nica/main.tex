\documentclass[10pt,a4paper]{article}

\usepackage{relazione}

\author{Veronica Sacchi\thanks{ver22albireo@gmail.com}}
\include{variabili.tmp}

%Numerazione subsection con lettere
\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\title{Esercitazione 07B: Oscillatore sinusoidale a ponte di Wien con OpAmp}
\begin{document}
	
\maketitle

\section*{Scopo e strumentazione}
Lo scopo dell'esperienza consiste nel costruire e caratterizzare un oscillatore ad onda sinusoidale a ponte di Wien.
La strumentazione utilizzata consiste di capacitori e resistitori, $2$ diodi \texttt{1N4148}, un OpAmp \texttt{TL081}, un potenziometro, un generatore di forme d'onda e uno di tensione. 
Per le misure sono stati utilizzati il multimetro digitale e l'oscilloscopio.


\section{Montaggio del circuito} %todo: decidi se fare la figura del circuito
Si è montato il circuito secondo lo schema mostrato nel testo dell'esercitazione, utilizzando la barra di distribuzione grigia per la tensione negativa, quella rossa per la tensione positiva, e quella nera per la massa.
Il circuito si compone di due parti:
\begin{itemize}
	\item[a)] un amplificatore non invertente di guadagno $A_V$, realizzato con l'OpAmp alimentato a $\pm \SI{15}{\volt}$, e con rete di feedback negativo costituita da una combinazione di diodi, resistitori e un potenziometro.
	\item[b)] una rete di feedback positivo dipendente dalla frequenza, costituito da un filtro passa basso composto da una combinazione di resistenze e capaci\`a, anch'esse riportate in figura.
\end{itemize}

Sono state misurate con il multimetro digitale le rispettive resistenze e capacità utilizzate. I risultati sono riportati qui di seguito:
\begin{align*}
	R_1 = \SI{\Runo}{\kilo\ohm}, \quad
	R_2 = \SI{\Rdue}{\kilo\ohm}, \quad
	&R_3 = \SI{\Rtre}{\kilo\ohm}, \quad
	R_4 = \SI{\Rquattro}{\kilo\ohm}, \\
	R_5 = \SI{\Rcinque}{\kilo\ohm}, \quad
	R_{p} = \SI{\Pot}{\kilo\ohm}, \quad
	&C_1 = \SI{\Cuno}{\nano\farad}, \quad
	C_2 = \SI{\Cdue}{\nano\farad}
\end{align*}

dove con $R_p$ si indica la resistenza totale del potenziometro usato.

\section{Misura del loop-gain $\beta A_V$ del circuito.}

Al fine di misurare il loop-gain del circuito il ramo di feedback \`e stato disconnesso dall'ingresso negativo dell'OpAmp, ingresso in cui veniva invece inviato un segnale sinusoidale di frequenza variabile e ampiezza $V_{in} \sim \SI{250}{\milli\volt}$. La tabella \ref{tab:bode} riporta le misure di ampiezza in ingresso, ampiezza in uscita dalla rete di sfasamento, sfasamento e guadagno al variare della frequenza.


%todo; forse voglio scrivere una didascalia pi\`u sofisfticata
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|c|c|c|c|c|}
			\hline
			$f[\si{\kilo\hertz}]$& $ V_{S_{pp}} [\si{\milli\volt}]$ & $V_{A_{pp}} [\si{\milli\volt}]$ & $\beta A_V[\si{\decibel}]$ & $\Delta\phi [\pi \si{\radian}]$\\
			\hline
			\input{tabella1.tmp}
			\hline
		\end{tabular}
		\caption{dati raccolti per studiare la risposta in frequenza del circuito, in particolare al fine di misurare il loop gain $\beta \cdot A_V$. Sia le misure delle ampiezze che quelle dello sfasamento sono state ottenute mediante l'uso dei cursori dell'oscilloscopio, mentre la frequenza \`e stata misurata attraverso la funzione apposita messa a disposizione dallo strumento, configurata nella modalit\`a di acquisizione che media su $128$ periodi.}
		\label{tab:bode}
	\end{center}
\end{table} 

Inoltre, per una maggiore intuizione visiva dell'andamento, i dati sono stati anche riportati in un diagramma di Bode che mostra i guadagno e lo sfasamento in funzione della frequenza, riportato in figura \ref{fig:bode}

\begin{figure}[h!]
	\centering
	\setcaptionwidth{0.80\linewidth}
	\caption{diagramma di Bode che mostra (a) l'ampiezza (in \texttt{Decibel})  e (b) lo sfasamento in funzione della frequenza, con le rispettive barre d'errore.}
	\label{fig:bode}
	\begin{subfigure}{0.81\textwidth}
		\caption{ampiezza}
		\includegraphics[width=\textwidth]{bodeampiezza.pdf}
		\label{fig:bodeamp}
	\end{subfigure}
	\begin{subfigure}{0.81\textwidth}
		\caption{sfasamento}
		\includegraphics[width=\textwidth]{bodesfasamento.pdf}
		\label{fig:bodesfa}
	\end{subfigure}
\end{figure}

Possiamo osservare che la figura \ref{fig:bodeamp} mostra il tipico andamento atteso \emph{a campana}, che ha un massimo a $\sim \SI{1.6}{\kilo\hertz}$, in corrispondenza del quale si ha anche sfasamento nullo entro le incertezze. Inoltre si pu\`o stimare la frequenza a cui si ha sfasamento nullo anche mettendo l'oscilloscopio in modalit\`a X-Y (quando uno dei due canali preleva il segnale all'ingresso del circuito e l'altro quello all'uscita della rete di feedback), osservando poi per quale frequenza il tipico ellisse degenera in un segmento. Con questo meotodo si \`e misurata:
\[
f_0 = \SI{1.607\pm0.007}{\kilo\hertz}
\]
mentre il valore che ci si attende teoricamente svolgendo i conti per la funzione di trasferimento del circuito vale:
\[
f_{0_{att}} = \frac{1}{2\pi} \cdot \sqrt{\frac{1}{R_1R_2C_1C_2}} = \SI{\fNatt}{\kilo\hertz}
\]
Notiamo che queste $3$ stime di $f_0$ sono incompatibili tra di loro, ma tutte compatibili con la frequenza attesa teoricamente entro $2\sigma$, effetto probabilmente dovuto alla difficolt\`a strumentale nel misurare gli sfasamenti con i cursori quando assumono valori molto vicini allo $0$, oltre a possibili deviazioni dal modello teorico di cui non si \`e tenuto conto.

\begin{figure}[h!]
	\centering
	\caption{screenshots dello schermo dell'oscilloscopio dove il canale $1$ prende il segnale di ingresso $V_S$ e il canale $2$ preleva il segnale in uscita dalla rete di sfasamento $V_A$. I tre diversi momenti corrispondono a diverse posizioni del potenziometro, stimata dal parametro $f$, definito come il rapporto tra la resistenza verso terra e la resistenza totale del potenziometro.}
	\label{fig:potenziometro}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f \sim 1$}
		\includegraphics[width=\textwidth]{dati/2-pot0-0.png}
		\label{fig:pot1}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f \sim 0.5$}
		\includegraphics[width=\textwidth]{dati/2-pot0-5.png}
		\label{fig:pot0.5}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f \sim 0$}
		\includegraphics[width=\textwidth]{dati/2-pot1-0.png}
		\label{fig:pot0}
	\end{subfigure}
\end{figure}

La figura \ref{fig:potenziometro} mostra, fissata l'onda in ingresso, come varia l'uscita dalla rete di feedback al variare della posizione del trimmer. \\
 In particolare, come atteso, osserviamo che, detto $f$ il rapporto tra la resistenza verso terra e la resistenza totale del potenziometro (e che quindi, per definzione, \`e compresa tra $0$ e $1$), minore \`e $f$ maggiore \`e l'ampiezza del segnale in uscita, dando un guadagno $\beta \cdot A_V \sim 1$ per $f \sim 0.5$, come mostra la figura \ref{fig:pot0.5}.
Da questa osservazione \`e anche chiaro come l'altezza del picco della curva di guadagno del diagramma di bode (come quello in \ref{fig:bodeamp}) dipenda fortemente dalla posizione del potenziometro.

\begin{figure}[h!]
		\begin{center}
			\includegraphics[width=0.77\textwidth]{AVs.pdf}
			\setcaptionwidth{0.77\linewidth}
			\caption{andamento del guadagno di loop-gain al variare dell'ampiezza del segnale in ingresso per frequenza di centro banda.}
			\label{fig:A-Vs}
		\end{center}
\end{figure}

Infine si \`e studiato l'andamento del guadagno in funzione dell'ampiezza dell'onda in ingresso, fissando la frequenza a quella a cui si ha sfasamento nullo tra ingresso e uscita (ossia $f_0 = \SI{1.607\pm0.007}{\kilo\hertz}$). Variando l'ampiezza del segnale $V_S$ e misurando l'ampiezza del segnale in uscita si sono ottenuti i dati riportati in tabella \ref{tab:A-Vs} e nel grafico \ref{fig:A-Vs}.

\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|c|c|c|}
			\hline
			$ V_{S_{pp}} [\si{\volt}]$ & $V_{A_{pp}} [\si{\volt}]$ & $\beta A_V[\si{\decibel}]$\\
			\hline
			\input{tabella2.tmp}
			\hline
		\end{tabular}
		\caption{dati raccolti per studiare la risposta del circuito al variare dell'ampiezza del segnale in ingresso, fissando la frequenza a quella a cui si registra uno sfasamento nullo tra i segnali di ingresso e uscita dalla rete di feedback. }
		\label{tab:A-Vs}
	\end{center}
\end{table} 

Osserviamo in figura~\ref{fig:A-Vs} che, come ci si attende, inizialmente, per ampiezze in ingresso piuttosto basse, il guadagno \`e costante, mentre oltre questo breve \emph{plateau} il guadagno diminuisce con un andamento approssimativamente esponenziale.

%\begin{figure}
%	\centering
%	\includegraphics[width=\textwidth]{example-image-duck}
%	\caption{segnali $V_{sh}$ (in verde) e $V_{out}$ (in azzurro) risultanti dalla simulazione del circuito al computer. Lo schema circuitale usato per la simulazione è riportato qui sotto, con anche i valori assegnati ai componenti circuitali. L'onda quadra in ingresso è rappresentata in arancione ed ha un'ampiezza selezionata di $\SI{6}{\volt}$ picco-picco. Il guadagno dell'OpAmp nella simulazione è $+\infty$.\\
%		La frequenza \emph{bandwidth} della simulazione è $\SI{100}{\mega \hertz}$.}
%	\includegraphics[width=0.9\textwidth]{example-image-duck}
%	\label{fig:paperelle}
%\end{figure}




\newpage % Così non si mischiano le due parti
\section{Analisi del funzionamento dell'oscillatore in funzione della posizione del potenziometro}
\begin{figure}[h!]
	\centering
	\caption{screenshots dello schermo dell'oscilloscopio dove il canale $1$ preleva il segnale $V_{out}$ all'uscita dell'OpAmp, mentre il canale $2$ preleva il segnale in uscita dalla rete di sfasamento $V_A$. I diversi momenti corrispondono a diverse posizioni del potenziometro, stimata dal parametro $f$, definito come il rapporto tra la resistenza verso terra e la resistenza totale del potenziometro. Le figure sono poste in ordine di $f$ decrescente, partendo dalla prima in cui si \`e molto vicini al punto di innesco fino ad arrivare all'ultima, in cui $f\sim 0$ e l'ampiezza dell'onda in uscita \`e limitata dalla saturazione dell'OpAmp.}
	\label{fig:oscillatore}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f$ vicina al punto di innesco}
		\includegraphics[width=\textwidth]{dati/3-finnesco.png}
		\label{fig:osc1}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		%\caption{Risposta del circuito con $f \sim 0.5$}
		\includegraphics[width=\textwidth]{dati/3-1.png}
		\label{fig:osc2}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		%\caption{Risposta del circuito con $f \sim 0$}
		\includegraphics[width=\textwidth]{dati/3-fbassa.png}
		\label{fig:osc3}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		%\caption{Risposta del circuito con $f \sim 0$}
		\includegraphics[width=\textwidth]{dati/3-facaso.png}
		\label{fig:osc4}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		%\caption{Risposta del circuito con $f \sim 0$}
		\includegraphics[width=\textwidth]{dati/3-fpiubassa.png}
		\label{fig:osc5}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f \sim 0$}
		\includegraphics[width=\textwidth]{dati/3-fbassissima.png}
		\label{fig:osc6}
	\end{subfigure}
\end{figure}
Come si pu\`o osservare dalla figura~\ref{fig:oscillatore} per $f$ inferiore al punto di innesco si osserva un oscillazione di frequenza sempre di $\sim \SI{1.6}{\kilo\hertz}$, mentre per $f$ superiore al punto di innesco non si osserva alcuna oscillazione ed entrambi i segnali rimangono piatti al valore $0$.
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|c|}
			\hline
			$ f [\si{\hertz}] $\\
			\hline
			\input{tabella3.tmp}
			\hline
		\end{tabular}
		\caption{dati raccolti per studiare la risposta del circuito al variare della posizione del potenziometro, con una $f$ inferiore al punto di innesco e decrescente in valore.}
		\label{tab:freq-potenz}
	\end{center}
\end{table} 

Come si osserva dalla tabella \ref{tab:freq-potenz} la frequenza dell'oscillazione non varia significatiavamente al variare della posizione del potenziometro, rimanendo sempre entro l'intervallo di confidenza della $f_0$ misurata al punto precedente (fatta eccezione per l'ultimo valore, che tuttavia non \`e rappresentativo del modello studiato perch\`e entrano in gioco gli effetti di non linearit\`a dell'OpAmp). \\
Una caratteristica che varia significativamente al variare della posizione del potenziometro \`e l'ampiezza del segnale, crescente per $f$ che decresce allontanandosi dal punto di innesco, fino ad osservare un chiaro comportamento non lineare nell'ultima figura \ref{fig:osc6}. In particolare si osserva un clipping del segnale in uscita dall'OpAmp a circa $\sim \SI{15}{\volt}$, ossia poco meno della tensione di alimentazione, dunque \`e presumbibile che si tratti proprio della saturazione dell'OpAmp che viene raggiunta in quanto il suo guadagno $A$ \`e troppo elevato in questa configurazione.

\section{Misura del guadagno dell'OpAmp}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{51.pdf}
	\caption{Punti sperimentali con relative barre d'errore racolti per la misura del guadagno dell'OpAmp, sovrapposti alla retta di best fit ottenuta usando come funzione modello una retta passante per l'origine.}
	\label{fig:guadagno1}
\end{figure}
Per misurare il guadadgno dell'OpAmp si \`e nuovamente disconnesso il ramo di feedback dall'ingresso negativo dell'amplificatore, dopo aver fissato la posizione del potenziometro quanto pi\`u vicino possibile a quella di innesco delle oscillazioni. Si \`e dunque proceduti a inviare un segnale in ingresso, misurandone poi l'ampiezza in uscita, ottenendo i dati riportati in tabella \ref{tab:guad}. \\
\begin{table}[h!]
	\begin{center}
		\begin{tabular}{|c|c|c|}
			\hline
			$ V_{in} [\si{\volt}]$  & $V_{out} [\si{\volt}]$ & $A_V [\si{\decibel}]$\\
			\hline
			\input{tabella5.tmp}
			\hline
		\end{tabular}
		\caption{dati raccolti al fine di studiare il guadagno dell'OpAmp nella posizione del potenziometro corrispondente all'innesco delle oscillazioni, atteso di $3 \simeq \SI{-9.54}{\decibel}$; possiamo gi\`a notare da questa tabella come nel caso preso in esame questo guadagno sia verificato solo per basse ampiezze in ingresso.}
		\label{tab:guad}
	\end{center}
\end{table} 
Per un'analisi pi\`u quantitativa si \`e eseguito un fit iterato con la funzione \texttt{curve\_fit} di \texttt{Python}, usando come funzione modello una retta passante per l'origine ed ottenendo i seguenti risultati:
\begin{align*}
	A & = \num{\Aattuno} \\
	\chi ^ 2 / \text{ndof} & = \chisquareduno / \ndofuno	
\end{align*}
Nella figura \ref{fig:guadagno1} sono stati riportati i punti sperimentali sovrapposti alla retta di best fit ottenuta con questo primo modello. Tuttavia il $\chi ^2$ molto elevato, come anche l'andamento dei punti sperimentali rispetto alla retta di best fit, lasciano supporre che il modello non sia molto accurato e in effetti dalle analisi precedenti, riassunte qualitativamente nella figura \ref{fig:A-Vs} ci si attende che il guadagno diminuisca per ampiezze sempre pi\`u elevate, e che rimanga vicino a $\sim 3$ solo per ampiezze in ingresso inferiori a $\sim \SI{2}{\volt}$, ipotesi verificata dai dati in tabella \ref{tab:guad}. \\
Non \`e cos\`i sorprendente dunque che il guadagno stimato dal fit sottostimi e non sia compatibile con quello atteso teoricamente in quanto la maggior parte dei punti sperimentali campiona una zona in cui si prevede che il guadagno effettivo sia inferiore a $3$; come gi\`a discusso invece, a basse ampiezze in ingresso i valori sperimentali sono compatibil con le previsioni teoriche.

\section{Funzionalit\`a dei diodi}

\begin{figure}[h!]
	\centering
	\caption{screenshots dello schermo dell'oscilloscopio dove il canale $1$ preleva il segnale $V_{out}$ all'uscita dell'OpAmp, mentre il canale $2$ preleva il segnale in uscita dalla rete di sfasamento $V_A$. I diversi momenti corrispondono a diverse posizioni del potenziometro, stimata dal parametro $f$, definito come il rapporto tra la resistenza verso terra e la resistenza totale del potenziometro. Le figure sono poste in ordine di $f$ crescente, partendo dalla prima in cui $f\sim 0$ e l'ampiezza dell'onda in uscita \`e limitata dalla saturazione dell'OpAmp, fino ad arrivare alle ultime due in cui si \`e molto vicini al punto di innesco. Nei primi due casi sono stati riportati anche i cursori delle misure di tensione per poter stimare le ampiezze del segnale $V_{out}$ nelle due diverse configurazioni.}
	\label{fig:diodi}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f \simeq 0$.}
		\includegraphics[width=\textwidth]{dati/6-f0misure.png}
		\label{fig:diodi1}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f$ intermedia tra il valore nullo e il punto di innesco delle ocillazioni.}
		\includegraphics[width=\textwidth]{dati/6-fintermedia.png}
		\label{fig:diodi2}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f$ vicina al punto di innesco}
		\includegraphics[width=\textwidth]{dati/6-innesco.png}
		\label{fig:diodi3}
	\end{subfigure}
	\begin{subfigure}{0.49\textwidth}
		\caption{Risposta del circuito con $f$ molto vicina al punto di innesco}
		\includegraphics[width=\textwidth]{dati/6-innesco2.png}
		\label{fig:diodi4}
	\end{subfigure}
\end{figure}

Ripristinato il collegamento tra l'ingresso negativo e il ramo di feedback sono stati questa volta tolti i diodi, ottenendo il segnale di auto-oscillazione riportato in figura \ref{fig:diodi}. 
Si vede quindi come i diodi diano stabilit\`a in ampiezza al segnale oscillante in quanto per ampiezze elevate abbassano il guadagno, evitando la saturazione dell'OpAmp; eliminando i diodi possiamo osservare un effetto di \emph{clipping} gi\`a nella semionda negativa per il segnale generato vicino al punto di innesco, effetto sempre pi\`u pronunciato man mano che viene diminuita la frazione di resistenza verso terra del potenziometro. \\
Notiamo infine la somiglianza tra le figure \ref{fig:osc6} e \ref{fig:diodi1}, dove entrambi i segnali presentano un \emph{clipping} ad un'ampiezza picco-picco di $\SI{28}{\volt}$, ma nel caso dei diodi l'ampiezza dell'onda originiaria (intesa come quella che si avrebbe se non ci fosse \emph{clipping}) risulta inferiore rispetto a quella ottenuta nel caso senza diodi; da questo confronto \`e chiaro quindi come i diodi limitino il guadagno per ampiezze grandi (in particolare sopra la tensione di soglia), aggiungendo una non linearit\`a che permette di avere segnali oscillatori stabili in ampiezza.


\section*{Dichiarazione}
Io, firmataria di questa relazione dichiaro che il contenuto della relazione \`e originale, con misure effettuate esclusivamente da me medesima.
	
\end{document}
