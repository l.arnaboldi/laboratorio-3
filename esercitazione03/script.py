# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *

# cose generali
latex_file = Document()
data, fond = np.loadtxt('dati/componenti.txt', unpack = True)

R1 = de2unc(data[0]*(1e3), 100, 0.8) # Converto in ohm
R2 = de2unc(data[1]*(1e3), 10, 0.8) # Converto in ohm
RE = de2unc(data[2]*(1e3), 1, 0.8) # Converto in ohm
RC = de2unc(data[3]*(1e3), 10, 0.8) # Converto in ohm
Cin = de2unc(data[4]/(1e9), 3e-9, 4) # Converto in farad
Cout = de2unc(data[5]/(1e9), 3e-10, 4) # Converto in farad
print('componenti')
print(R1, R2, RE, RC, Cin, Cout)
# Salvataggio componenti
latex_file.setvariable(UDecimal('Runo', R1/(1e3)))#in kiloohm
latex_file.setvariable(UDecimal('Rdue', R2/(1e3)))#in kiloohm
latex_file.setvariable(UDecimal('RE', RE/(1e3))) #in kiloohm
latex_file.setvariable(UDecimal('RC', RC/(1e3)))#in kiloohm
latex_file.setvariable(UDecimal('Cin', Cin*1e9)) #in nanoFarad
latex_file.setvariable(UDecimal('Cout', Cout*1e9)) #in nanoFarad

# puunto 2
data, fond = np.loadtxt('dati/2.txt', unpack = True)
V_cc = de2unc(data[0], 0.1, 0.5)
V_RC = de2unc(data[1], 0.001, 0.5)
V_E = de2unc(data[2], 0.01, 0.5)
V_B = de2unc(data[3], 0.001, 0.5)
V_BE = de2unc(data[4], 0.001, 0.5)
V_C = de2unc(data[5], 0.01, 0.5)
V_CE = de2unc(data[6], 0.01, 0.5)
print('voltaggi punto 2')
print(V_cc, V_RC, V_B, V_BE, V_C, V_CE)
print()
print()
V_BB = V_cc / (1. + R1 / R2)
I_C_q = (V_BB - V_BE) / RE
print('I_C_q:', I_C_q)
I_C_s = V_RC / RC
print('I_C_s:', I_C_s)
V_CE_q = V_cc - I_C_q * (RC + RE)
print('V_CE_q:', V_CE_q)
print('V_CE:', V_CE)

I_B = V_E / RE - I_C_s
sprint(I_B)

# trucco del donato
beta = unc.ufloat(100., 0.)
RB = R1 * R2 / (R1 + R2)
I_C_q_donato = (V_BB - V_BE) / (RE + RB / beta)
V_CE_q_donato = V_cc - I_C_q_donato * (RC + RE)
sprint(RB)
sprint(I_C_q_donato)
sprint(V_CE_q_donato)

# altre cose
I_par = V_cc/(R1 + R2)
sprint(I_par)
V_B_th = V_BB
V_E_th = RE * I_C_q_donato
V_BE_th = V_B_th - V_E_th
V_C_th = V_cc - RC * I_C_q_donato


latex_file.setvariable(UDecimal('Vcc', V_cc))
latex_file.setvariable(UDecimal('VRC', V_RC))
latex_file.setvariable(UDecimal('VE', V_E))
latex_file.setvariable(UDecimal('VB', V_B))
latex_file.setvariable(UDecimal('VBE', V_BE))
latex_file.setvariable(UDecimal('VC', V_C))
latex_file.setvariable(UDecimal('VCE', V_CE))
latex_file.setvariable(UDecimal('VBB', V_BB))
latex_file.setvariable(UDecimal('ICq', I_C_q * 1e3)) #milliampere
latex_file.setvariable(UDecimal('ICs', I_C_s * 1e3)) #milliampere
latex_file.setvariable(UDecimal('VCEq', V_CE_q))
latex_file.setvariable(UDecimal('VCEq', V_CE_q))
latex_file.setvariable(UDecimal('ICqdonato', I_C_q_donato * 1e3)) #milliampere
latex_file.setvariable(UDecimal('VCEqdoanto', V_CE_q_donato))
latex_file.setvariable(UDecimal('IB', I_B * 1e3)) #milliampere
latex_file.setvariable(UDecimal('Ipar', I_par * 1e3)) #milliampere
latex_file.setvariable(UDecimal('VBth', V_B_th))
latex_file.setvariable(UDecimal('VEth', V_E_th))
latex_file.setvariable(UDecimal('VBEth', V_BE_th))
latex_file.setvariable(UDecimal('VCth', V_C_th))


# punto 3a3
V_in_, V_out_ = np.loadtxt('dati/3a3.txt', unpack = True)
V_in = de2unc(V_in_, 0., 3)
V_out = de2unc(V_out_, 0., 3)
G = V_out / V_in

def guadagno(x, A):
    return x * A

def derivata(x, A):
    return A

A = iterated_fit(guadagno, V_in, V_out, df = derivata, absolute_sigma = False)

sprint(A)
latex_file.setvariable(UDecimal('Guad', A[0]))

residual_plot(
    f = guadagno,
    param = A,
    X=V_in,
    Y=V_out,
    use_ux = True,
    df = derivata,
    xlabel = '$V_{in}$ [$\\si{\\volt}$]',
    ylabel = '$V_{out}$ [$\\si{\\volt}$]',
    figfile = 'guadagno.pdf',
)

chiquadro, ndof = chi2iterated(guadagno, A, V_in, V_out, df = derivata)
sprint(chiquadro)
sprint(ndof)
latex_file.setvariable(Decimal('GuadChiQudro', chiquadro))


# punto 4
f_, df, V_in_, V_out_ = np.loadtxt('dati/4a.txt', unpack = True)
f = de2unc(f_, df, 5e-3)
V_in = de2unc(V_in_, 0., 3)
V_out = de2unc(V_out_, 0., 3)
A = V_out / V_in

tabella1 = TabularContent()
tabella1.add_column(f)
tabella1.add_column(A)
tabella1.save('tabella1.tmp.tex')

def bode(x, f_limit_1, f_limit_2, a1, b1, a2, b2, c):
    def f(x_):
        if x_ < f_limit_1:
            return unp.pow(x_, a1) * unp.exp(b1)
        elif x_ > f_limit_2:
            return unp.pow(x_, a2) * unp.exp(b2)
        else:
            return c #unc.ufloat(c, 0.)
    tmp = [f(x_) for x_ in x]
    try:
        return unp.uarray(*tmp)
    except:
        return np.array(tmp)

def bode_retta(x, a1, b1):
    return unp.pow(x, a1) * unp.exp(b1)

def bode_costante(x, c):
    return c

p1 = ucurve_fit(bode_retta, f[:5], A[:5], absolute_sigma = False)
p2 = ucurve_fit(bode_costante, f[5:11], A[5:11], absolute_sigma = False)
p3 = ucurve_fit(bode_retta, f[-3:], A[-3:], absolute_sigma = False)

chisq1, ndof1 = chi2(bode_retta, p1, f[:5], A[:5])
chisq2, ndof2 = chi2(bode_costante, p2, f[5:11], A[5:11])
chisq3, ndof3 = chi2(bode_retta, p3, f[-3:], A[-3:])

print(chisq1)
print(chisq2)
print(chisq3)



# frequenze limite
f_limit_l = umath.pow(p2[0] / umath.exp(p1[1]), 1. / p1[0])
f_limit_h = umath.pow(p2[0] / umath.exp(p3[1]), 1. / p3[0])

# frequenze di taglio
f_T_passaH = umath.pow(p2[0] / numpy.sqrt(2.) / umath.exp(p1[1]), 1. / p1[0])
f_T_passaL = umath.pow(p2[0] / numpy.sqrt(2.) / umath.exp(p3[1]), 1. / p3[0])

sprint(f_T_passaH)
sprint(f_T_passaL)
print(p2[0])

p = (f_limit_l, f_limit_h) + p1 + p3 + p2

residual_plot(
    f = bode,
    param = p,
    X=f,
    Y=A,
    xlogscale = True,
    ylogscale = True,
    use_ux = False,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$A$',
    figfile = 'bode.pdf',
)

latex_file.setvariable(UDecimal('fTpassaH', f_T_passaH))
latex_file.setvariable(UDecimal('fTpassaL', f_T_passaL))
latex_file.setvariable(UDecimal('guadagnoalcentro', p2[0]))

# tanti conti che porco dio eeeeeee
h_ie = 4.4e3
h_fe = 100.
Rin = 1. / (1. / R1 + 1. / R2 + 1. / (h_ie + h_fe * RE))
sprint(Rin)
fLth = 1. / (2 * numpy.pi * Cin * Rin)
sprint(fLth)

Rosc = 1e6
Rout = 1. / (1. / (h_ie + h_fe * RE) + 1. / RC + 1. / Rosc)
c_eb = 80e-12
c_cb = 25e-12
Ct = c_cb + c_eb# + Cin
fHth = 1. / (2 * numpy.pi * Ct * RC)
sprint(fHth)
print(1. / (2 * numpy.pi * Cout * Rosc))

# cose finali
latex_file.save('variabili.tmp.tex')
