# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *


# cose generali
latex_file = Document()

# Parte A
print('________PARTE A________')
data = np.loadtxt('dati/componentiA.txt', unpack = True)
R1 = de2unc(data[0], 1, 0.8)
R2 = de2unc(data[1], 10, 0.8)
latex_file.setvariable(UDecimal('RunoA', R1/1000.))
latex_file.setvariable(UDecimal('RdueA', R2/1000.))
sprint(R1)
sprint(R2)
A_att = -R2 / R1
latex_file.setvariable(UDecimal('AattA', A_att))
sprint(A_att)
print()

#punto 1c
f, df = np.loadtxt('dati/freq1c.txt', unpack = True)
F = de2unc(f, df, 50/(1e6))
sprint(F)
v_in, v_out = np.loadtxt('dati/1c.txt', unpack = True)
V_in = de2unc(v_in, 0., 3)
V_out = de2unc(v_out, 0., 3)

def guadagno(x, A):
    return A * x

def derivata(x, A):
    return A

A = iterated_fit(guadagno, V_in, V_out, df = derivata, absolute_sigma = False) #TODO capire se l'erroe ci piace di più con True o False

sprint(A)
latex_file.setvariable(UDecimal('AmisA', A[0]))

chiquadro, ndof = chi2iterated(guadagno, A, V_in, V_out, df = derivata)
sprint(chiquadro)
sprint(ndof)
latex_file.setvariable(Decimal('GuadChiQuadroA', chiquadro))
latex_file.setvariable(Decimal('GuadndofA', ndof, digits = 1))

residual_plot(
    f = guadagno,
    param = A,
    X=V_in,
    Y=V_out,
    use_ux = True,
    df = derivata,
    xlabel = '$V_{in}$ [$\\si{\\volt}$]',
    ylabel = '$V_{out}$ [$\\si{\\volt}$]',
    figfile = 'guadagnoA.pdf',
)

# tabella dei dati
tab1 = TabularContent()
tab1.add_column(V_in)
tab1.add_column(V_out)
tab1.add_column(V_out/V_in)
tab1.save('tabella1.tmp.tex')

print()

# punto 2a
f, v_in, v_out = np.loadtxt('dati/bode2a.txt', unpack = True)
f = de2unc(f, 0., 1.) #TODO: metti un errore sensato
V_in = de2unc(v_in, 0., 3)
V_out = de2unc(v_out, 0., 3)
A = V_out / V_in

gbw = de2unc(4e6, 0., 0.) # parametro delle specifiche
fT_att = gbw / (2 * math.pi * (-A_att))
sprint(fT_att)
latex_file.setvariable(UDecimal('fTattesaA', fT_att / 1000.))


# tabella dei dati
tab2 = TabularContent()
tab2.add_column(f/1000.)
tab2.add_column(V_in)
tab2.add_column(V_out)
tab2.add_column(A)
tab2.save('tabella2.tmp.tex')

def passa_basso(x, A0, fT):
    return A0 / unp.sqrt(1 + (x / fT)**2)
    
p = ucurve_fit(passa_basso, f, A, absolute_sigma = True)

A0, fT = p
sprint(A0)
sprint(fT)
latex_file.setvariable(UDecimal('fTbodeA', fT / 1000.))
latex_file.setvariable(UDecimal('GuadagnobodeA', A0))

chiquadro, ndof = chi2(passa_basso, p, f, A)
sprint(chiquadro)
sprint(ndof)
latex_file.setvariable(Decimal('chiquadrobodeA', chiquadro))
latex_file.setvariable(Decimal('ndofbodeA', ndof, digits = 2))

residual_plot(
    f = passa_basso,
    param = p,
    X=f,
    Y=A,
    xlogscale = True,
    ylogscale = True,
    use_ux = False,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$A$',
    figfile = 'bode2a.pdf',
)
print()

# parte 2b

data = np.loadtxt('dati/slew_rate.txt', unpack = True)
delta_V = de2unc(data[0], 0., 3)
delta_t = de2unc(data[1], 0., 3) #TODO: controlla che siano sensate

SR = delta_V / delta_t
sprint(SR)
latex_file.setvariable(UDecimal('slewrate', SR))


print()


# Parte B
print('________PARTE B________')
data = np.loadtxt('dati/componentiB.txt', unpack = True)
R1 = de2unc(data[0], 1, 0.8)
R2 = de2unc(data[1], 10, 0.8)
C = de2unc(data[2], 3e-10, 4)
sprint(R1)
sprint(R2)
sprint(C)
fT_att = 1. / (2 * math.pi * R2 * C)
sprint(fT_att)

latex_file.setvariable(UDecimal('RunoB', R1/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('RdueB', R2/1000.)) #in kiloOhm
latex_file.setvariable(UDecimal('C', C*1.e9)) #in nano Farad
latex_file.setvariable(UDecimal('freqtaglioattesaB', fT_att))
latex_file.setvariable(UDecimal('guadagnoattesoB', R2/R1))



f, v_in, v_out, sfa = np.loadtxt('dati/bode3a.txt', unpack = True)
f = de2unc(f, 0., 1.) #TODO: metti un errore sensato
delta_t = de2unc(sfa, 0., 1.) #TODO: metti un errore sensato
delta_phi = 2 * math.pi * delta_t * f
#delta_phi = delta_phi - math.pi
V_in = de2unc(v_in, 0., 3)
V_out = de2unc(v_out, 0., 3)
A = V_out / V_in

# tabella dei dati
tab3 = TabularContent()
tab3.add_column(f/1000.)
tab3.add_column(V_in)
tab3.add_column(V_out)
tab3.add_column(A)
tab3.add_column(delta_t * 1e6)
tab3.add_column(delta_phi/math.pi)
tab3.save('tabella3.tmp.tex')

# bode ampiezze
def bode_retta(x, a1, b1):
    return unp.pow(x, a1) * unp.exp(b1)

def bode_costante(x, c):
    return c

def bode(x, f_limit, a1, b1, c):
    def f(x_):
        if x_ > f_limit:
            return bode_retta(x_, a1, b1)
        else:
            return c
    tmp = [f(x_) for x_ in x]
    try:
        return unp.uarray(*tmp)
    except:
        return np.array(tmp)
        
p1 = ucurve_fit(bode_costante, f[9:], A[9:], absolute_sigma = False) # qui stiamo barando parecchio, ma non lo diremo (abbiamo arbitrariamente tolto un punto dal fit...)
p2 = ucurve_fit(bode_retta, f[3:-5], A[3:-5], absolute_sigma = False)

#vogliono la pendenza della retta
pendenza, quota = p2
pendenzadecibel = pendenza * 20.
sprint(pendenzadecibel)
sprint(quota)
latex_file.setvariable(UDecimal('pendenzabodeB', pendenzadecibel))

outliers = [True] * 3 + [False] * 5 + [True] + [False] * 4

# f limite e taglio
f_taglio = umath.pow(p1[0] / umath.exp(p2[1]), 1. / p2[0])
sprint(f_taglio)
latex_file.setvariable(UDecimal('ftagliobodeB', f_taglio))

latex_file.setvariable(UDecimal('guadagnobodeB', p1[0]))

#chiquadro etc
chiquadro, ndof = chi2(bode, (f_taglio,) + p2 + p1, f[3:], A[3:])
sprint(chiquadro)
sprint(ndof)
latex_file.setvariable(Decimal('chiquadroampiezzeB', chiquadro))
latex_file.setvariable(Decimal('ndofampiezzeB', ndof, digits = 1))

print("Provaiamo a fittare un passa basso")
A0, fT = ucurve_fit(passa_basso, f[3:], A[3:])
sprint(fT)
latex_file.setvariable(UDecimal('guadagnopassabassoB', A0))
latex_file.setvariable(UDecimal('freqtagliopassabassoB', fT))
chiquadro, ndof = chi2(passa_basso, (A0, fT), f[3:], A[3:])
latex_file.setvariable(Decimal('chiquadroampiezzepassabassoB', chiquadro))
latex_file.setvariable(Decimal('ndofampiezzepassabassoB', ndof, digits = 1))

residual_plot(
    f = bode,
    #second_f = passa_basso,
    #second_param = (A0, fT),
    param = (f_taglio,) + p2 + p1,
    X=f,
    Y=A,
    outliers = outliers,
    xlogscale = True,
    ylogscale = True,
    use_ux = False,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$A$',
    figfile = 'bode3a.pdf',
)

residual_plot(
    f = passa_basso,
    param = (A0, fT),
    X=f[3:],
    Y=A[3:],
    xlogscale = True,
    ylogscale = True,
    use_ux = False,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$A$',
    figfile = 'bode3apassabasso.pdf',
)

print()

# bode sfasamenti

def sfasamento(x, fT):
    return unp.arctan(-x/fT) / math.pi + 1.
    
fT, = ucurve_fit(sfasamento, f[3:], delta_phi[3:] / math.pi, p0 = fT_att.n)
chiquadro, ndof = chi2(sfasamento, (fT,), f[3:], delta_phi[3:] / math.pi)
sprint(fT)
sprint(chiquadro)
sprint(ndof)
latex_file.setvariable(UDecimal('fTsfasamentoB', fT))
latex_file.setvariable(Decimal('chiquadrosfasamentoB', chiquadro))
latex_file.setvariable(Decimal('ndofsfasamentoB', ndof, digits = 1))


residual_plot(
    f = sfasamento,
    param = (fT,),
    X=f[3:],
    Y=delta_phi[3:] / math.pi,
    xlogscale = True,
    ylogscale = False,
    use_ux = False,
    xlabel = '$f$ [$\\si{\\hertz}$]',
    ylabel = '$\\Delta \\varphi [\pi]$',
    figfile = 'bode3asfa.pdf',
)

# punto 3b
f, v_in, v_out = np.loadtxt('dati/3b.txt', unpack = True)
f = de2unc(f, 0., 1.)
V_in = de2unc(v_in, 0., 3.)
V_out = de2unc(v_out, 0., 3.)

V_out_att = V_in / (4 * R1 * C * f)
sprint(f)
sprint(V_out_att)
sprint(V_out)
latex_file.setvariable(UDecimal('freqtrebB', f / 1000.))
latex_file.setvariable(UDecimal('vintrebB', V_in))
latex_file.setvariable(UDecimal('vouttrebB', V_out * 1000.))
latex_file.setvariable(UDecimal('voutatttrebB', V_out_att * 1000.))




# cose finali
latex_file.save('variabili.tmp.tex')
