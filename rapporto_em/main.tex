\documentclass[10pt,a4paper]{article}

\usepackage{relazione}
\usepackage{animate}


\include{variabili.tmp}

\captionsetup{width=1.2\linewidth}

%Numerazione subsection con lettere
\renewcommand{\thesubsection}{\thesection.\alph{subsection}}

\author{
	Gruppo B28 \\
	Luca Arnaboldi\thanks{luca@arnaboldi.lu},
	Francesco Loparco\thanks{francesco.loparco@live.it},
	Veronica Sacchi\thanks{ver22albireo@gmail.com}
}


\title{Misura rapporto carica massa per l'elettrone \\ \texttt{versione telematica}}

\begin{document}
	\maketitle
	
	\section{Scopo dell'esperienza e strumentazione}
	Lo scopo di questa esperienza \`e misurare il rapporto carica/massa dell'elettrone; a tale fine si analizzano le fotografie fornite dai docenti per ricavare una stima del raggio delle traiettorie, applicando due metodi di analisi, descritti in modo pi\`u specifico in seguito.\\
	Data la modalit\`a di svolgimento telematica l'unico materiale di cui si \`e fatto uso sono le immagini fornite.
	
	
	\section{Calibrazione}
	Un primo importante punto di riflessione concerne la calibrazione del sistema, necessaria a convertire le unit\`a di \emph{pixel} con cui vengono forniti i dati, in unit\`a di lunghezza funzionali ai confronti e alle analisi successive. Un punto importante da tenere in considerazione \`e l'effetto di geometria proiettiva che si viene a creare in questa configurazione, dove ci troviamo a confrontare oggetti su piani paralleli a distanze diverse, e che l'immagine acquisita dalla fotocamera schiaccia su un unico piano.
	
	Per prima cosa si calibrano indipendentemente le due scale relative ai piani su cui giacciono i due righelli, come mostrati nelle immagini \texttt{img11} e \texttt{img13}: per ciascuna delle due sono stati campionati i punti in corrispondenza delle tacche della scala considerata, associando un'incertezza assoluta pari alla semilarhezza (in \emph{pixel}) della singola tacca (nel caso del righello giallo anteriore quindi si \`e usata un'incertezza uniforme di $2$ \emph{pixel}, mentre per quello posteriore di $1$ solo \emph{pixel}). Dopodich\`e \`e stato possibile effettuare un fit dei minimi quadrati prendendo a modello la funzione lineare:
	\[
	d [\mathit{pixel}]= \zeta \cdot D[\si{\centi\meter}] 
	\]
	dove le distanze in \texttt{centimetri} sono state assunte senza incertezza.\\
	Il fattore di conversione cercato risulta quindi essere $1/\zeta$ e i risultati per i due fit sono:
	\begin{align*}
		\frac{1}{\zeta _A} &= \SI{\pxtocmdavanti}{\centi\meter}/\mathit{pixel}\\
		(\chi ^2/ \text{ndof})_A&= \chiquadrodavanti/\num{\ndofdavanti}\\
		\frac{1}{\zeta _P} &= \SI{\pxtocmdietro}{\centi\meter}/\mathit{pixel}\\
		(\chi ^2/ \text{ndof})_P&= \chiquadrodietro/\num{\ndofdietro}
	\end{align*}
	dove il pedice ``$A$'' sta per \emph{anteriore}, e il pedice ``$P$'' per \emph{posterioire}. Gli andamenti dei dati sovrapposti alla retta di best fit sono riportati nei grafici \ref{fig:calib}.
	
	\begin{figure}
		\centering
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\caption{retta di best fit (blu) sovrapposta ai punti campionati sulla scala anteriore, con relative barre di errore, utilizzati per effettuare la calibrazione della scala pi\`u vicina all'obiettivo della fotocamera.}
			\label{fig:calib-dav}
			\includegraphics[width = \textwidth]{calib-dav.pdf}
			\vspace*{0.3cm}
		\end{subfigure}
		\hfill
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\caption{retta di best fit (blu) sovrapposta ai punti campionati sulla scala posteriore, con relative barre di errore, utilizzati per effettuare la calibrazione della scala pi\`u lontana dall'obiettivo della fotocamera.}
			\label{fig:calib-die}
			\includegraphics[width = \textwidth]{calib-die.pdf}
			\vspace*{0.3cm}
		\end{subfigure}
		\caption{rette di fit utilizzate per calcolare il fattore di conversione da \emph{pixel} a \texttt{centimetri}, necessaria a convertire le lunghezze misurate sulle fotografie in unit\`a utili alle analisi successive.}
		\label{fig:calib}
	\end{figure}
	
	Possiamo osservare che entrambi i $\chi ^2$ ridotti sono molto minori di $1$ ed incompatibili con esso: dovendo escludere l'overfitting potremmo attribuire questo fenomeno ad una sovrastima delle incertezze di misura; dal \emph{plot} dei residui in figura \ref{fig:calib-die} evidenziamo un sospetto andamento periodico, che sembra indicare un forte errore sistematico di cui non si \`e tenuto conto.\\
	\`E possibile che questo fenomeno sia dovuto al fatto che la scala di misura in \emph{pixel} \`e discreta (con un'unit\`a fondamentale - appunto - di $1$ \emph{pixel}), mentre le grandezze che si vogliono misurare sono continue - e non necessariamente multiple intere di $1$ \emph{pixel}. Ne segue che potendo misurare sullo schermo solo distanze multiple di pixel, spostandosi lungo la scala (continua del righello), passando di tacca in tacca si introduce sempre uno stesso scarto. Appena la somma degli scarti introdotti supera la larghezza di un unità fondamentale si viene riportati al di sotto del valore corretto: questo produce l'andamento a dente di sega che si riscontra negli errori. \\
	Sottolineamo che attraverso la calibrazione con il fit questo effetto tende a cancellarsi nel parametro risultante, mentre se si fosse fatta la calibrazione con una sola misura  l'errore sistematico sarebbe rimasto e quindi si sarebbe ottenuta una calibrazione peggiore.
	
	A questo punto, attraverso le relazioni della geometria proiettiva, \`e possibile trovare il fattore di conversione da \emph{pixel} a \texttt{centimetri} per il piano che contine la traiettoria degli elettroni, essendo note le distanze relative tra questi tre piani paralleli, ottenendo:
	
	\[
	\frac{1}{\zeta} = \frac{\frac{D_2}{\zeta_A}+ \frac{D_1}{\zeta_P}}{D_1 + D_2} = \SI{\pxtocm}{\centi\meter}/\mathit{pixel}
	\]
	dove $D_1$ e $D_2$ sono le distanze del piano dell'orbita degli elettroni rispettivamente dal righello anteriore e da quello posteriore.\\
	Osserviamo che questa trattazione trascura totalmente la rifrazione data dalla presenza del bulbo, tuttavia sovrapponendo la scala posteriore nell'immagine \texttt{img13} (in assenza del bulbo) a quelle delle immagini \texttt{img12} o \texttt{img03} (un esempio \`e portato in \ref{fig:bulbo})si nota che non sembrano esserci effetti rilevanti nella regione centrale, in cui generalmente cade l'orbita degli elettroni; si \`e dunque deciso di trascurarla in una prima analisi, e di commentare in seguito i pochi casi in cui gli elettroni si muovono vicino al bordo del bulbo, dove invece gli effetti di distorsione sono rilevanti.\\
	Non \`e detto che una piccola distorsione sulla scala posteriore implichi una piccola distorsione della traiettoria (ad esempio la ``doppia'' rifrazione che subisce il raggio provieniente dalla scala posteriore potrebbe contribuire a compensare l'effetto), e infatti consideriamo questa solo un primo approccio al problema: analisi pi\`u quantitative verranno proposte nei commenti finali.
	
	\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{bulbo-scala.jpeg}
		\caption{confronto tra le scale posteriori delle immagini \texttt{img03} e \texttt{img13}, la prima delle quali \`e rifratta dal bulbo. Sovrapponendo l'immagine a una griglia si pu\`o osservare come gli effetti di distorisione non siano sensibilmente apprezzabili nella zona tra $-\SI{5}{\centi\meter}$ e $\SI{5}{\centi\meter}$ circa, in cui tipicamente si colloca il fascio di elettroni.}
		\label{fig:bulbo}
	\end{figure}
	
	\newpage
	\section{Acquisizione dei punti e misura del raggio}
	Per campionare la traiettoria degli elettroni sono state annotate le coordinate in \emph{pixel} di diversi punti della traccia, avendo cura di campionare in modo pi\`u fitto le regioni in cui lo spessore del pennello fosse pi\`u fine, rimanendo invece pi\`u radi (o in alcuni casi non campionando affatto) le zone in cui il pennello fosse pi\`u spesso. \\
	In questo modo si assegna un peso diverso ai vari punti, privilegiando quelli in cui la traiettoria pu\`o essere individuata pi\`u precisamente.\\
	Fissato un set di dati, per determinare la circonferenza di \emph{best-fit} si \`e fatto uso di due metodi: il primo prevede il campionamento di tre punti (scelti arbitrariamente ma avendo cura di massimizzare qualitativamente la lunghezza delle corde individuate) e, in modo equivalente a quanto suggerito nel testo dell'esperienza, stimando il raggio della circoscritta attraverso la formula
	\begin{equation}
		r = \frac{l_1 \cdot l_2 \cdot l_3}{4 \cdot A}
		\label{eq:metodo1}
	\end{equation}
	dove $l_1, l_2, l_3$ sono le lunghezze delle $3$ corde e $A$ \`e l'area del triangolo da queste definito (facilmente calcolabile computazionalmente mediante la valutazione del \emph{determinante} della matrice $3 \times 2$ delle coordinate). \\
	Il secondo metodo invece \`e quello che vede l'esecuzione di un \emph{fit circolare} come descritto nel documento \texttt{circle\_fit.pdf}, fornito dai docenti insieme al materiale a disposizione. Al fine di avere una stima della bont\`a del fit si \`e definita la quantit\`a 
	\begin{equation}
		\Upsilon = \frac{1}{N}\sum_{i}^{N} \frac{\sqrt{(x_i -x_c)^2 + (y_i-y_c)^2}}{r}
		\label{eq:upsilon}
	\end{equation}
	dove $N$ \`e il numero di punti, $(x_i,y_i)$ le coordinate del $i-$esimo punto, $(x_c,y_c)$ le coordinate del centro ed $r$ il raggio come determinati dal fit; ci si aspetta quindi che questa quantit\`a sia tanto pi\`u vicina a $1$ quanto migliore \`e il fit.
	\\
	Per ogni figura si \`e campionata la circonferenza individuata dal bordo interno del pennello e quella data dal bordo esterno, si \`e determinato il raggio di entrambe in modo indipendente, prendendo infine come valore medio ed incertezza associata al raggio la media aritmetica e la semidispersione dei due valori di \emph{best-fit} trovati.
	Nelle tabelle che seguono sono riportati per ogni figura i valori dei raggi e delle quantit\`a rilevanti a ricavare il rapporto carica massa secondo la formula:
	\begin{equation}
		\frac{e}{m} = \frac{2V_{acc}}{(B_zr)^2}
		\label{eq:e-m}
	\end{equation}
	La tabella \ref{tab:metodoI} \`e relativa alle stime ottenute usando il \emph{I metodo}, mentre la tabella \ref{tab:metodoII} si riferisce alle misure ottenute stimando i raggi delle traiettorie attraverso il \emph{II metodo}.
	
	
	\begin{landscape}
		\begin{table}[h!]
			\begin{center}
				\begin{tabular}{c|}
					Immagine \\
					\hline
					\hline
					$1$ \\
					$2$\\
					$3$\\
					$4$\\
					$5$\\
					$6$\\
					$7$\\
					$8$\\
					$9$\\
					$10$
				\end{tabular}%
				\begin{tabular}{|c|c|c|c|c|c}
					
					$r_{min}[\SI{}{\centi\meter}]$ &  $r_{max}[\SI{}{\centi\meter}]$ &  $r[\SI{}{\centi\meter}]$ &$2V_{acc}[\si{\volt}]$ & $(B_z\cdot r)^2[(\si{\tesla\centi\meter})^2]$ & $e/m[\si{\coulomb/\kilo\gram}]$\\
					\hline
					\hline
					\input{tabella1.tmp}
					
				\end{tabular}
				\caption{dati relativi al primo metodo di determinazione dei raggi delle traiettorie. I pedici \emph{min} e \emph{max} si riferiscono rispettivamente all cerchio minimo e e al cerchio massimo che approssimano la traiettoria.}
				\label{tab:metodoI}
			\end{center}
		\end{table}

		\begin{table}[h!]
			\begin{center}
				\begin{tabular}{c|}
					Immagine \\
					\hline
					\hline
					$1$ \\
					$2$\\
					$3$\\
					$4$\\
					$5$\\
					$6$\\
					$7$\\
					$8$\\
					$9$\\
					$10$
				\end{tabular}%
				\begin{tabular}{|c|c|c|c|c|c|c|c|c|c}
					
					$r_{min}[\SI{}{\centi\meter}]$ & $1-\Upsilon _{min}$ & nop$_{min}$ & $r_{max}[\SI{}{\centi\meter}]$ & $1-\Upsilon _{max}$ & nop$_{max}$ & $r[\SI{}{\centi\meter}]$ &$2V_{acc}[\si{\volt}]$ & $(B_z\cdot r)^2[(\si{\tesla\centi\meter})^2]$ & $e/m[\si{\coulomb/\kilo\gram}]$\\
					\hline
					\hline
					\input{tabella2.tmp}
					
				\end{tabular}
				\caption{dati relativi al secondo metodo di determinazione dei raggi delle traiettorie. I pedici \emph{min} e \emph{max} si riferiscono rispettivamente all cerchio minimo e e al cerchio massimo che approssimano la traiettoria. Inoltre le colonne intestate da $\Upsilon$ riportano la stima della quantit\`a definita in \eqref{eq:upsilon}, mentre quelle intestate da \emph{nop} riportano il numero di punti campionati per il rispettivo caso.}
				\label{tab:metodoII}
			\end{center}
		\end{table}
	\end{landscape}
	
	
	
	\section{Calcolo del rapporto $e/m$}
	
	A questo punto \`e possibile calcolare il rapporto $\frac{e}{m}$ come media dei valori delle singole immagini, in  modo che sia minimizzato il $\chi^2$; mediando quindi i valori della tabella \ref{tab:metodoI} o della tabella \ref{tab:metodoII} otteniamo rispettivamente i valori:
	\begin{align}
		\left(\frac{e}{m}\right)_I &= \SI{\emTrePuntiMedia}{\coulomb/\kilo\gram}\\
		\left(\frac{e}{m}\right)_{II}&= \SI{\emFitMedia}{\coulomb/\kilo\gram}
		\label{eq:rismetodo1}
	\end{align}
    In Figura~\ref{fig:e-m-raggio} è consultabile una rappresentazione grafica del metodo.

	Il secondo approccio proposto per ottenere una stima del rapporto cercato a partire dai valori dei raggi misurati nelle diverse immagini prevede un fit lineare per stimare il coefficiente angolare della funzione 
	\begin{equation}
		2V_{acc} = \frac{e}{m} \cdot (B_z\cdot r)^2.
		\label{eq:fit-function}
	\end{equation}
	A seconda che la stima dei raggi derivi dal \emph{metodo I} o dal \emph{metodo II} otteniamo rispettivamente i risultati
	\begin{align*}
		\left(\frac{e}{m}\right)_{III} &= \SI{\emTrePuntiFit}{\coulomb/\kilo\gram}\\
		\chi ^2 / \text{ndof} &= \num{\chiquadrtrepunti}/\ndoftrepunti \\
		\\
		\left(\frac{e}{m}\right)_{III} &= \SI{\emFitFit}{\coulomb/\kilo\gram}\\
		\chi ^2 / \text{ndof} &= \num{\chiquadrfit}/\ndoffit
	\end{align*}
	i cui andamenti sono stati riportati rispettivamente nei grafici \ref{fig:trepunti} e \ref{fig:fit}.
	Per tener conto delle rilevanti incertezze sull'asse delle ascisse si \`e eseguito un fit iterato mediante la funzione \texttt{curve\_fit} di \texttt{Python}.
	
		\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{TrePuntiFit.pdf}
		\caption{raffronto tra retta di best fit (in blu) e punti sperimentali con relative barre d'errore, le cui ascisse derivano dalle stime dei raggi delle varie immagini effettuate con il \emph{metodo I}, ossia intersecando gli assi di due corde della circonferenza.}
		\label{fig:trepunti}
	\end{figure}

	\begin{figure}
	\centering
	\includegraphics[width=0.9\textwidth]{FitFit.pdf}
	\caption{raffronto tra retta di best fit (in blu) e punti sperimentali con relative barre d'errore, le cui ascisse derivano dalle stime dei raggi delle varie immagini effettuate con il \emph{metodo II}, ossia eseguendo un fit circolare su diversi punti campionati, come descritto nella sezione precedente.}
	\label{fig:fit}
\end{figure}
	\newpage
	
	\section{Commenti finali}
	
	Il valore tabulato per il rapporto carica-massa dell'elettrone vale\footnote{secondo quanto indicato dal NIST in \url{https://physics.nist.gov/cgi-bin/cuu/Value?esme}} (in modulo)
	\begin{equation}
		\left(\frac{e}{m}\right)_{tabulata} = \SI{1.758 820 010 76(53) e11}{\coulomb/\kilo\gram}
		\label{eq:val-tabulato}
	\end{equation}
	
	Osserviamo che le stime ottenute sono tra loro tutte compatibili, ma non lo sono con il valore tabulato \eqref{eq:val-tabulato}, e si ha un fattore $\sim 2$ tra il valore tabulato e le nostre stime. 
	Analizziamo i possibili effetti di alcuni errori sistematici che potrebbero avere un ruolo.
	
	\subsubsection*{Dipendenza di $e/m$ dal raggio della traiettoria}
	La presenza di effetti sistematici pu\`o essere evidenziata da una dipendenza di $\frac{e}{m}$ dalla stima del raggio della traiettoria da cui deriva. Per metterla in luce osserviamo i grafici riportati in \ref{fig:e-m-raggio}: qualunque sia la stima utilizzata per il raggio della traiettoria possiamo osservare un andamento prettamente discendente all'aumentare del raggio, con i primi due valori che si collocano a ben pi\`u di $5\sigma$ dal valore mediato.\\
	Ci sono diversi effetti che potrebbero rendere conto di questo comportamento inatteso: innanzitutto la rifrazione del bulbo \`e stata totalmente trascurata, mentre ci aspettiamo che corregga la misura del raggio di un fattore dipendente dal raggio stesso, ma che tuttavia ci aspettiamo pi\`u rilevanti per raggi grandi (un raggio proveniente direttamente dal centro della sfera non subirebbe alcuna deviazione); inoltre la superficie del bulbo (esterna, ma soprattutto interna) molto probabilmente \`e scabra, con effetti rifrattivi di difficile previsione; infine lo spessore del pennello \`e sempre approssimativamente lo stesso, indipendentemente dal raggio, ma naturalmente l'errore relativo associato sar\`a pi\`u rilevante per raggi piccoli.\\
	Osserviamo poi che per raggi piccoli il centro del cerchio non pu\`o essere approssimato come molto vicino al centro del bulbo, e la circonferenza rimane molto spostata vero il bordo inferiore, quindi i punti diversi della traiettoria gli elettroni subiscono un campo magnetico diverso, oltre al fatto che gli effetti di rifrazione dovuto al bulbo contribuiscono a distorcere ancora di pi\`u la forma della traccia, rendendo le nostre analisi ancora meno affidabili; questi effetti sono propri e molto pi\`u rilevanti per i primi due raggi, dunque sembra ragionevole attribuirne la deviazione consistente rispetto agli altri punti, evidenzata in figura \ref{fig:e-m-raggio}.
	
	
	\begin{figure}
		\centering
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\caption{media pesata (blu) sovrapposta ai punti sperimentali, con relative barre di errore, che confrontano l'andamento di $\frac{e}{m}$ in funzione del raggio delle traiettoria, calcolato con il \emph{metodo I}.}
			\label{fig:e-m-raggioI}
			\includegraphics[width = \textwidth]{media-em-t.pdf}
			\vspace*{0.3cm}
		\end{subfigure}
		\hfill
		\begin{subfigure}[t]{0.49\textwidth}
			\centering
			\caption{media pesata (blu) sovrapposta ai punti sperimentali, con relative barre di errore, che confrontano l'andamento di $\frac{e}{m}$ in funzione del raggio delle traiettoria, calcolato con il \emph{metodo II}.}
			\label{fig:e-m-raggioII}
			\includegraphics[width = \textwidth]{media-em-f.pdf}
			\vspace*{0.3cm}
		\end{subfigure}
		\caption{andamento di $\frac{e}{m}$ al variare del raggio della traiettorie campionate.}
		\label{fig:e-m-raggio}
	\end{figure}
	
	\subsubsection*{Effetti dovuti al campo magnetico terrestre}
	Il campo magnetico terrestre a Pisa vale $\sim\SI{50}{\micro\tesla}$, mentre il campo magnetico generato dalle bobine \`e dell'ordine del $\si{\milli\tesla}$, con un errore dell'ordine dell'$1-3\%$, per cui il fondo \`e confrontabile con l'incertezza di misura; dato che differisce di circa $2$ ordini di grandezza rispetto al modulo del campo di interesse il suo contributo tanto al modulo, quanto alla direzione del campo totale pu\`o essere considerato trascurabile.
	Per un confronto pi\`u quantitativo osserviamo che, assunta l'orientazione della strumentazione come schematizzata nella scheda di consegna, nel caso del campo magnetico pi\`u basso con cui si \`e lavorato passiamo da $B_z =\SI{780\pm17}{\micro\tesla}$ a $B_z =\SI{781\pm17}{\micro\tesla}$, e dunque si hanno effetti del tutto trascurabili rispetto all'incertezza associata alla misura per altri effetti.
	
	\subsubsection*{Variazione radiale del campo magnetico}
	La variazione del campo magnetico lungo l'asse $\hat{z}$ viene totalmente trascurata in quanto il grafico in figura $1$ del file \texttt{esperienza\_2020\_telematica.pdf} mostra che non ci sono variazioni sensibili entro i $\sim\SI{2}{\centi\meter}$, che \`e molto maggiore dell'incertezza associata al piano di rotazione degli elettroni.
	\newline
	Per quanto riguarda la variazione del campo magnetico legata alla distanza dall'asse di rotazione osserviamo dal grafico in figura $2$ del file \texttt{esperienza\_2020\_telematica.pdf} che entro i $\SI{4}{\centi\meter}$ la variazione \`e $\sim 1\%$, da confrontare con l'errore compreso tra $1\%$ e $3\%$ associato a $B$; effettivamente questo effetto potrebbe gi\`a produrre variazioni sensibili, ma di certo non pu\`o spiegare la grandissima discrepanza che si riscontra tra i valori di $\frac{e}{m}$ ricavati e quello tabulato.
	
	\subsubsection*{Correzioni dovute alla rifrazione attraverso il bulbo}
		\begin{figure}
		\centering
		\includegraphics[width=0.9\textwidth]{bulbo-rifrazione.pdf}
		\caption{schema dell'apparato strumentale, con definizione delle grandezze tipiche utilizzate. I cerchi concentrici rappresentano le due superfici del bulbo di raggio $R$ ed $r$, mentre l'osservatore \`e immaginato al vertice dell'angolo $\theta$; gli angoli $\alpha$ sono gli angoli di incidenza e rifrazione del raggio reale, mentre $D$ \`e la distanza tra osservatore e centro del bulbo.}
		\label{fig:bulbo-diff}
	\end{figure}
	Utilizzando lo schema in figura \ref{fig:bulbo-diff} \`e stato possibile calcolare lo spostamento del raggio dovuto alla rifrazione del bulbo
	
	\begin{align*}
		\sin\gamma&= \left( \sqrt{\left[1 - \left(\frac{n_1}{n_2}\frac{D}{r}\sin\theta\right)^2\right]\cdot \left[1 - \left(\frac{D}{R}\sin\theta\right)^2\right]} - \frac{n_1}{n_2}\frac{D}{r}\frac{D}{R}\sin^2\theta \right) \cdot\\
		&\cdot \left(\cos\theta \sqrt{1 - \left(\frac{n_1}{n_2}\frac{D}{r}\sin\theta\right)^2} - \frac{n_1}{n_2}\frac{D}{R}\sin^2\theta \right) +\\
		&+\left(\frac{n_1}{n_2}\frac{D}{R}\sin\theta\cos\theta + \sin\theta\sqrt{1 - \left(\frac{n_1}{n_2}\frac{D}{r}\sin\theta\right)^2}  \right) \cdot \\
		&\left(\frac{n_1}{n_2}\frac{D}{r}\sin\theta\sqrt{1 - \left(\frac{D}{R}\sin\theta\right)^2} + \frac{D}{R}\sin\theta\sqrt{1 - \left(\frac{n_1}{n_2}\frac{D}{r}\sin\theta\right)^2} 
		\right)\\
		\\
		\Delta h &= \frac{D\sin\theta}{\frac{D}{r}\sin\theta\cos\gamma + \sin\gamma\sqrt{1-\left(\frac{D}{r}\sin\theta\right)^2}}
	\end{align*}

	dove $n_1$ ed $n_2$ sono gli indici di rifrazione di aria e vetro, $R$ ed $r$ sono i raggi interni ed esterni del bulbo, $D$ \`e la distanza tra osservatore e centro del bulbo e $\theta$ \`e l'angolo sotto cui viene \emph{vista} la traccia (di porebbe dire che \`e il \emph{raggio angolare}). Da questa descrizione si ricava immediatamente che $\theta$ \`e legato ai raggi ``apparenti'' $x$ delle traiettorie dalla relazione:
	\[
	\tan\theta = \frac{x}{D}
	\]
	mentre $D$ \`e ricavabile dai fit eseguiti sulle calibrazioni delle due scale, come descritto in sezione $1$, e vale:
	\[
	D = \SI{\Distanzafotocamera}{\centi\meter}.
	\]
	Fissiamo infine i parametri:
	\[
	n_1\sim \num{1}\quad n_2\sim \num{\ifvetro} \quad \quad R= \SI{\Resterno}{\centi\meter} \quad r= \SI{\rinterno}{\centi\meter}
	\]
	che sono ragionevoli nel sistema utilizzato (per essere precisi la stima di $R - r$ \`e probabilmente esagerata, dunque la nostra \`e probabilmente una sovrastima delle deviazioni dovute alla rifrazione) e otteniamo, usando le stime dei raggi delle traiettorie ottenute con il \emph{metodo II} la tabella \ref{tab:bulbo}.
	
	\begin{table}[h!]
		\begin{center}
			\begin{tabular}{c|c}
				
				$r[\si{\centi\meter}]$ & $\Delta h [\si{\centi\meter}]$\\
				\hline
				\hline
				\input{tabellabulbo.tmp}
				
			\end{tabular}
			\caption{spostamento dato dalla rifrazione del bulbo a partire dai raggi ``apparenti'' sotto cui appare la traiettoria, stimati attraverso il \emph{metodo II}. \`E da notare che in tutte queste stime si \`e assunta l'orbita centrata attorno al centro del bulbo, un'ipotesi ragionevole per raggi abbastanza grandi ma non verificata dalle due circonferenze di raggio minore.}
			\label{tab:bulbo}
		\end{center}
	\end{table}
	
	In tutti i casi l'effetto della rifrazione \`e di circa un ordine di grandezza inferiore rispetto all'incertezza associata alla stima del raggio, dunque \`e un effetto che possiamo considerare trascurabile.
	
	\subsubsection*{Conclusioni}
	Tutti gli effetti fin qui analizzati introducono correzioni dell'ordine del $1\%$ sulle stime effettuate; altri effetti che non sono stati analizzati in dettaglio includono una possibile inclinazione del piano dell'orbita degli elettroni rispetto all'osservatore o la scabrosit\`a delle superifici, che tuttavia dovrebbe risultare trascurabile, e in ogni caso sono tutti effetti che non possono rendere conto del fattore $2$ di differenza riscontrato tra le nostre stime e il valore tabulato.\\
	Riteniamo quindi che sia presente un importante effetto sistematico che non \`e stato possibile individuare (limitazione forse dovuta anche alla modalit\`a di lavoro telematica); dalle analisi precedenti osserviamo che il \emph{plot} dei residui del grafico \ref{fig:calib-die} sembra evidenziare un possibile effetto di questo tipo che dovrebbe essere ulteriormente indagato, e ancora di p\`u i $\chi ^2$ dei fit lineari utilizzati per stimare $\frac{e}{m}$, risultano insolitamente elevati.\\
	A questo proposito i grafici \ref{fig:trepunti} e \ref{fig:fit} sembrano denunciare un andamento s\`i lineare, ma con un \emph{offset} non banale, e dunque in una successiva prova pratica potrebbe essere opportuno misurare con pi\`u cura i valori delle correnti e dei voltaggi, verificando con cautela che sorgenti di campo esterne o accoppiamenti spuri di ogni genere siano trascurabili.
	
	\section*{Dichiarazione}
	I firmatari di questa relazione dichiarano che il contenuto della relazione \`e originale, 
	e che tutti i firmatari hanno contribuito alla elaborazione della relazione stessa.
	
	
	
\end{document}


