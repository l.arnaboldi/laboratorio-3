# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *
from LabTools.measure import *


#cose generali
latex_file = Document()
tester = Tester('../multimetro_digitale.yaml')
osc = Oscilloscope('../oscilloscopio.yaml')

print('________PARTE 2________')
data = np.loadtxt('dati/2.txt', unpack = True)
V_CC = tester.voltage(data[0])
R1 = tester.resistance(data[1])
R2 = tester.resistance(data[2])
sprint(V_CC)
latex_file.setvariable(UDecimal('Vccdue', V_CC)) #in V
sprint(R1)
latex_file.setvariable(UDecimal('Runodue', R1/1000.)) #in kOhm
sprint(R2)
latex_file.setvariable(UDecimal('Rduedue', R2)) #in Ohm


Vin, Vout = np.loadtxt('dati/2-inout.txt', unpack = True)
Vin = tester.voltage(Vin)
Vout = tester.voltage(Vout)

tab1 = TabularContent()
tab1.add_column(Vin)
tab1.add_column(Vout)
tab1.save('tabella1.tmp.tex')

errorbars_plot(
    Vin,
    Vout,
    xlabel = '$V_\\text{in}$ [$\\si{\\volt}$]',
    ylabel = '$V_\\text{out}$ [$\\si{\\volt}$]',
    figfile = '2.pdf',
)
# Fitto la transizione
def f(x, A, B):
    return A*x+B
def df(x, A, B):
    return A
A, B = iterated_fit(f, Vin[12:21], Vout[12:21], df = df)

SH = unc.ufloat(2.4, 0.)
SL = unc.ufloat(0.4, 0.)

VIH = (SL - B) / A
VIL = (SH - B) / A
sprint(VIL)
latex_file.setvariable(UDecimal('VILdue', VIL)) #in V
latex_file.setvariable(UDecimal('VIHdue', VIH)) #in V
sprint(VIH)

# Costruisco a mano il grafico che vogliamo
x, ux = unpack_unarray(Vin)
y, uy = unpack_unarray(Vout)
rcConfig()
fig = plot.figure()
fig.set_size_inches([10., 7.5])
main = plot.subplot2grid((12,9), (0,0), colspan = 9, rowspan = 12, fig = fig)
main.set_xlim(xlimit(x, ux, False))
main.minorticks_on()
grid = np.linspace(1.2, 1.61, 1000)
ry, schifo = unpack_unarray(f(grid, A, B))
main.plot(grid,ry, zorder=3, lw=0.4)
main.plot([-1,10], [0.4]*2, c = 'red', lw = 0.2, linestyle = '--')
main.plot([-1,10], [2.4]*2, c = 'green', lw = 0.2, linestyle = '--')
main.errorbar(x, y, xerr = ux, yerr = uy, zorder=5, **{
    'linestyle' : '',
    'c' : 'k',
    'capsize' : 0,
    'elinewidth' : 0.7,
})
main.set_xlabel('$V_\\text{in}$ [$\\si{\\volt}$]')
main.set_ylabel('$V_\\text{out}$ [$\\si{\\volt}$]')
savepdf(fig, '2_.pdf')

print('________PARTE 3________')
data = np.loadtxt('dati/3.txt', unpack = True)

Vcc = tester.voltage(data[0])
latex_file.setvariable(UDecimal('Vcctre', Vcc)) #in V
R = tester.resistance(data[1])
latex_file.setvariable(UDecimal('Rtre', R/1000.)) #in kOhm
f = osc.frequency(data[2], fond = 500e-9)
latex_file.setvariable(UDecimal('freqtre', f)) #in Hz
V_IL = osc.voltage(data[3], fond = 16.)
latex_file.setvariable(UDecimal('VILtre', V_IL)) #in V
V_IH = osc.voltage(data[4])
latex_file.setvariable(UDecimal('VIHtre', V_IH)) #in V
V_OL = osc.voltage(data[5])
latex_file.setvariable(UDecimal('VOLtre', V_OL)) #in V
V_OH = osc.voltage(data[6])
latex_file.setvariable(UDecimal('VOHtre', V_OH)) #in V
tPHL = osc.time(data[7], fond = 50e-9)
latex_file.setvariable(UDecimal('tPHLtre', tPHL*1.e9)) #in ns
tPLH = osc.time(data[8], fond = 250e-9)
latex_file.setvariable(UDecimal('tPLHtre', tPLH*1.e9)) #in ns

print('________PARTE 4________')
data, labels = np.loadtxt('dati/4.txt', unpack = True, dtype = 'float, S2')
Vcc = tester.voltage(data[0])
Vpin = tester.voltage(data[1:])
latex_file.setvariable(UDecimal('Vccquattro', Vcc)) #in V

tab2 = TabularContent()
tab2.add_column(Vpin)
tab2.save('tabella2.tmp.tex')




# cose finali
latex_file.save('variabili.tmp.tex')
