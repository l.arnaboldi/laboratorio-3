# coding=utf-8
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc
import uncertainties.umath as umath
import math

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *

# Fin tanto che LabTools non è sviluppato completamente usiamo le versioni vecchie
#from lab1.lab_tool import ucurve_fit, MLS
#from lab2.lab_tool import chi2MLS

def att_passa_basso(f, f_t):
    return 1. / unp.sqrt(1. + unp.pow(f / f_t, 2.))

def att_passa_basso_model(f, f_t):
    return 1. / np.sqrt(1 + np.power(f / f_t, 2.))

# cose generali
latex_file = Document()
data, fond = np.loadtxt('dati/RC.txt', unpack = True)

R1 = de2unc(data[0]*(1e3), 10, 0.8) # Converto in ohm
C1 = de2unc(data[1]/(1e9), 30e-12, 4) # Converto in farad
R2 = de2unc(data[2]*(1e3), 10, 0.8) # Converto in ohm
C2 = de2unc(data[3]/(1e9), 30e-12, 4) # Converto in farad

# punto 1
print('__________________1___________________')
print('R1: ', R1)
print('C1: ', C1)
latex_file.setvariable(UDecimal('Runo', R1/(1e3)))
latex_file.setvariable(UDecimal('Cuno', C1*1e9))

f_t = 1 / (2 * math.pi) * 1 / (R1 * C1)
print('f_T: ', f_t)
latex_file.setvariable(UDecimal('ftpuntouno', f_t/1000))

Av_duek = att_passa_basso(2e3, f_t)
Av_ventik = att_passa_basso(20e3, f_t)
print('attenuazione a 2kHz: ', Av_duek)
print('attenuazione a 20kHz: ', Av_ventik)
latex_file.setvariable(UDecimal('Avduek', Av_duek))
latex_file.setvariable(UDecimal('Avventik', Av_ventik))

# punto 2
print('__________________2___________________')
f_, df, V_in_, V_out_ = np.loadtxt('dati/bode.txt', unpack = True)
f = de2unc(f_, df, 0.005)
V_in = de2unc(V_in_, 0., 3.)
V_out = de2unc(V_out_, 0., 3.)
A = V_out / V_in
#print('A:')
#print(A)

tabella1 = TabularContent()
tabella1.add_column(f / 1000.) #kilohertz
tabella1.add_column(A)
tabella1.save('tabella1.tmp.tex')

p = ucurve_fit(att_passa_basso, f, A, absolute_sigma = False)
print('Fit -- f_t:', p[0])
latex_file.setvariable(UDecimal('ftfit', p[0]))

residual_plot(
    f = att_passa_basso,
    param = p,
    X=f,
    Y=A,
    use_ux = False,
    xlogscale = True,
    ylogscale = True,
    xlabel = '$f$ [$\si{\hertz}$]',
    ylabel = '$A_v$',
    figfile = 'bode.pdf',
)

print('Intersecando rette')
def retta(x, A, B):
    return unp.pow(x, A) * unp.exp(B)
def cost(x, A):
    return A

retta_x = f[-4:]
print(retta_x)
retta_y = A[-4:]
p_retta = ucurve_fit(retta, retta_x, retta_y, absolute_sigma = False)
print('Fit retta:', p_retta)

cost_x = f[:3]
print(cost_x)
cost_y = A[:3]
p_cost = ucurve_fit(cost, cost_x, cost_y, absolute_sigma = False)
print('Fit cost:', p_cost)
f_T = umath.pow(p_cost[0] / umath.exp(p_retta[1]), 1. / p_retta[0])
print(f_T)
latex_file.setvariable(UDecimal('ftintersezione', f_T))


# cose finali
latex_file.save('variabili.tmp.tex')
