
\section{Riproduzione dell'esperienza a casa}
Avendo a disposizione gli strumenti necessari si è provato a riprodurre l'esperienza a casa, con qualche adattamento.

\subsection*{Materiale a disposizione}
Il materiale a disposizione si compone di:
\begin{itemize}
	\item calibro ventesimale;
	\item laser a diodi da $\SI{5}{\milli\watt}$ con lunghezza d'onda nominale di $\SI{405\pm10}{\nano\meter}$;
	\item livella laser;
	\item metro a nastro.
\end{itemize}

\subsection*{Acquisizione dei dati}
Si è posto il laser su un tavolo in modo che incidesse radente sulla scala millimetrica del calibro, e che il raggio riflesso venisse raccolto su una parete. Si è poi posizionata la livella laser facendo sì che il livello fosse all'esatta altezza del calibro: così facendo sulla parete usata come schermo, oltre che il raggio riflesso, è presente una linea che segna il lo zero per le nostre misure. Il sistema è mostrato in Figura~\ref{fig:setup}.
\begin{figure}[h]
	\centering
	\caption{alcune fotografie del setup dell'esperimento.}
	\label{fig:setup}
	\begin{subfigure}[b]{0.45\textwidth}
		\caption{la livella laser traccia una linea alla stessa altezza in tutta la stanza}
		\includegraphics[width=\textwidth]{dati/livella.jpg}
	\end{subfigure}
	~
	\begin{subfigure}[b]{0.45\textwidth}
		\caption{dettaglio del posizionamento del laser e del calibro. Si  noti come la linea verde sia esattamente all'altezza del piano di riflessione}
		\includegraphics[width=\textwidth]{dati/sultavolo.jpg}
	\end{subfigure}
\end{figure}

Si è posizionato un foglio di carta bianca sulla parete su cui incideva il raggio riflesso, in modo da poter segnare con una matita il livello dello zero e le posizioni delle figure di diffrazione. Per avere una visione quanto più chiara possibile si è scelto di lavorare in una stanza buia. Il pattern formatosi sullo schermo è visibile in Figura~\ref{fig:figura}.
\begin{figure}[h]
	\centering
	\caption{figura di diffrazione formatasi sulla parete. Il punto più luminoso è l'ordine 0. Sono quindi visibili anche l'ordine -1 e l'ordine -2, oltre che parecchi ordini positivi.\\
		La linea verde rappresenta l'altezza della parte superiore del calibro, proiettata con la livella laser.}
	\label{fig:figura}
	\includegraphics[width=0.6\textwidth]{dati/figura.jpg}
\end{figure}
Si è misurata anzitutto la distanza $D$ tra calibro e parete con il metro a nastro. Come descritto in precedenza si è considerato l'ampiezza dello spot sul calibro come $3\sigma$ siccome molto probabilmente il valore corretto cade all'interno della regione illuminata.
Queste considerazioni hanno portano a \[D=\SI{\Dself}{\centi\meter}.\]
Successivamente per ogni riga della figura di diffrazione si è segnato il punto ad altezza minima e il punto ad altezza massima, per avere una stima più accurata dell'errore su $h_i$. Inoltre si è considerato un ulteriore errore di $\SI{1}{\milli\meter}$ dovuto sia allo spessore della linea della livella - che determina la linea di riferimento con una certa incertezza - sia all'errore commesso nel suo posizionamento. L'analisi svolta sui dati ricalca perfettamente quanto esposto in precedenza, compreso modello e metodo di fit. I dati sono riportati in Tabella~\ref{tab:self}.
\begin{table}
	\begin{center}
		
		\begin{tabular}{c|}
			
			Ordine ($m$) \\
			\hline
			\hline
			$-2$\\
			$-1$\\
			$0$\\
			$1$\\
			$2$\\
			$3$\\
			$4$\\
			$5$\\
			$6$\\
			$7$\\
			$8$\\
			$9$\\
			$10$\\
			$11$\\
			$12$\\
			$13$\\
			$14$\\
			$15$\\
			$16$\\
			$17$\\
			$18$\\
			$19$\\
			$20$\\
			$21$\\
			$22$\\
			$23$\\
			$24$\\
			$25$\\
			$26$\\
		\end{tabular}%
		\begin{tabular}{|c|c|c}
			
			$h_i[\si{\centi\meter}]$ & $\theta _d[\si{\radian}]$  & $\sin{\theta _d}$\\
			\hline
			\hline
			\input{tab1self.tmp}
		\end{tabular}
		\caption{dati raccolti a casa.}
		\label{tab:self}
	\end{center}
\end{table}

Il fit ha prodotto i seguenti risultati, rappresentati anche nel grafico di Figura \ref{fig:graficoself}.
\begin{align}
\lambda &=\SI{\lambdalaserself}{\nano\meter}\\
\theta_i &= \SI{\thetaiself}{\radian}\\
\chi ^2 / \text{ndof} &= \num{\chiself} / \num{\ndofself}.
\label{eq:A04}
\end{align}
\begin{figure}
	\centering
	\includegraphics[width=\textwidth]{graficoself.pdf}
	\caption{punti sperimentali con relative barre d'errore e retta di best-fit, con relativo grafico dei residui per il fit eseguito sui dati riportati in tabella~\ref{tab:self}.}
	\label{fig:graficoself}
\end{figure}
Notiamo che il valore di $\theta_i$ risultante dal fit è compatibile con il valore di $\theta_d$ per $m=0$, come ci si attende, dato che quest'ultimo non è altro che l'angolo di riflessione (e per le proprietà dell'ottica geometrica è uguale all'angolo di incidenza). Infine il valore di $\lambda$ ottenuto è pienamente compatibile con la lunghezza d'onda dichiarata dai produttori del laser.

I risultati del fit sono completamente compatibili, anche se in questo caso si ritrova di nuovo un $\chi^2$ ridotto minore di $1$, ma comunque più grande rispetto ai dati forniti. Anche l'errore relativo su $\lambda$ è più piccolo se confrontato con quelli precedenti. Questo potrebbe voler dire che la procedura di stima degli errori da noi utilizzata è pi\`u raffinata rispetto a quella adottata dagli altri gruppi di cui abbiamo analizzato i dati. Sicuramente l'utilizzo della livella laser ha permesso la rimozione dell'errore sistematico sullo 0 di cui si è parlato prima, ma non risolve tutti i problemi dovuti ad un eventuale non ortogonalità tra piano di lavoro e parete. Infine essendo un laser a diodo quello utilizzato per l'esperimento a casa, che quindi ha un'incertezza intrinseca sulla lunghezza d'onda, si potrebbero avere effetti dovuti alla diversa risposta dei materiali a lunghezze d'onda differenti. Ad esempio si è notato che il colore dello spot su una parete bianca o sul foglio bianco varia visibilmente, sintomo che i materiali interagisco in maniera diversa con il fascio laser.
















