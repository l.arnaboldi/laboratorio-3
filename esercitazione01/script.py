
import numpy as np
from uncertainties import unumpy as unp
import uncertainties as unc

from LabTools.utils import *
from LabTools.latex import *

# Fin tanto che LabTools non è sviluppato completamente usiamo le versioni vecchie
from lab1.lab_tool import ucurve_fit, MLS
from lab2.lab_tool import plotter, chi2MLS

def model(x, a):
    return a * x
    
def d_model(x, a):
    return a
    
def array_tensioni(v, fondoscala):
    digit_v = np.empty_like(fond)
    err_v = np.empty_like(fond)
    
    for i in range(0, len(fond)):
        if fond[i] == 2.:
            digit_v[i] = 0.001
            err_v[i] = 0.5
        elif fond[i] == 20.:
            digit_v[i] = 0.01
            err_v[i] = 0.5
        else:
            raise TypeError
    
    return de2unc(v, digit_v, err_v)
    
# cose generali
latex_file = Document()
    
################ 2B ############################################################
v_in, v_out, fond = np.loadtxt('dati/dat1.txt', unpack = True)
R, fondR = np.loadtxt('dati/resistenze1.txt', unpack = True)

R2 = de2unc(R[0]*1e3, 1, 0.8)
R1 = de2unc(R[1]*1e3, 1, 0.8)
A = R1 / (R1 + R2)

latex_file.setvariable(UDecimal('Runob', R1, unc_perc_accuracy = 15))
latex_file.setvariable(UDecimal('Rdueb', R2, unc_perc_accuracy = 15))
latex_file.setvariable(UDecimal('Ab', A))


V_in = array_tensioni(v_in, fond)
V_out = array_tensioni(v_out, fond)
#print(V_out)
#print(V_in)
G = V_out/V_in
#print(G)

tabella1 = TabularContent()
tabella1.add_column(V_in)
tabella1.add_column(V_out)
tabella1.add_column(V_out/V_in)
tabella1.save('tabella1.tmp.tex')

param = ucurve_fit(model, V_in, V_out)
#print(param)
param_ite = MLS(model, d_model, V_in, V_out, p0 = param)
#print(param_ite)

latex_file.setvariable(UDecimal('calb', param_ite[0]))

p , up = unpack_unarray(param_ite)
x, ux = unpack_unarray(V_in)
y, uy = unpack_unarray(V_out)
plotter(
    f = model,
    param = p,
    x=x,
    y=y,
    uy=uy,
    ux=ux,
    df = d_model,
    xlabel = '$V_{in}$ [V]',
    ylabel = '$V_{out}$ [V]',
    figfile = 'calibrazioneb.pdf',
)
chisq, ndof = chi2MLS(model, d_model, p, x, y, ux, uy)
latex_file.setvariable(Decimal('chisqb', chisq))
latex_file.setvariable(Variable('ndofb', ndof))

A = param_ite[0]
print(A)
R_tinv = (R2 - A * (R1 + R2)) / (A * R1 * R2)
print(R2 - A * (R1 + R2))
print(R_tinv)
print(1 / (R_tinv.n - R_tinv.s))
latex_file.setvariable(UDecimal('Rtkiloinv', R_tinv))
latex_file.setvariable(Decimal('Rtkilo', 1 / (R_tinv.n - R_tinv.s)))
print(1/A - (1 + R1/ R2))

## 2C ##########################################################################
v_in, v_out, fond = np.loadtxt('dati/dat2.txt', unpack = True)
R, fondR = np.loadtxt('dati/resistenze2.txt', unpack = True)

R1 = de2unc(R[0]*1e6, 1e4, 1.)
R2 = de2unc(R[1]*1e6, 1e4, 1.)
A = R1 / (R1 + R2)

latex_file.setvariable(UDecimal('Runoc', R1/1e6, unc_perc_accuracy = 15))
latex_file.setvariable(UDecimal('Rduec', R2/1e6))
latex_file.setvariable(UDecimal('Ac', A))

V_in = array_tensioni(v_in, fond)
V_out = array_tensioni(v_out, fond)
#print(V_out)
#print(V_in)
G = V_out/V_in
#print(G)

tabella2 = TabularContent()
tabella2.add_column(V_in)
tabella2.add_column(V_out)
tabella2.add_column(V_out/V_in)
tabella2.save('tabella2.tmp.tex')

param = ucurve_fit(model, V_in, V_out)
#print(param)
param_ite = MLS(model, d_model, V_in, V_out, p0 = param)
#print(param_ite)

latex_file.setvariable(UDecimal('calc', param_ite[0]))

p , up = unpack_unarray(param_ite)
x, ux = unpack_unarray(V_in)
y, uy = unpack_unarray(V_out)
plotter(
    f = model,
    param = p,
    x=x,
    y=y,
    uy=uy,
    ux=ux,
    df = d_model,
    xlabel = '$V_{in}$ [V]',
    ylabel = '$V_{out}$ [V]',
    figfile = 'calibrazionec.pdf',
)
chisq, ndof = chi2MLS(model, d_model, p, x, y, ux, uy)
latex_file.setvariable(Decimal('chisqc', chisq, ))
latex_file.setvariable(Variable('ndofc', ndof))

A = param_ite[0]
R_t = (A * R1 * R2) / (R2 - A * (R1 + R2))
latex_file.setvariable(UDecimal('Rtmega', R_t/(1e6)))


# oscc #########################################################################
v_in, v_out = np.loadtxt('dati/oscilloscopio.txt', unpack = True)
V_in = de2unc(v_in, 0., 3.2)
V_out = de2unc(v_out, 0., 3.2)

G = V_out/V_in

param = ucurve_fit(model, V_in, V_out)
#print(param)
param_ite = MLS(model, d_model, V_in, V_out, p0 = param)
#print(param_ite)



tabella3 = TabularContent()
tabella3.add_column(V_in)
tabella3.add_column(V_out)
tabella3.add_column(V_out/V_in)
tabella3.save('tabella3.tmp.tex')

A = param_ite[0]
R_t = (A * R1 * R2) / (R2 - A * (R1 + R2))
print(R_t)
latex_file.setvariable(UDecimal('Rtoscillo', R_t/(1e6)))

# freq #########################################################################
maxt, mint, freq_trig, f_osc = np.loadtxt('dati/misure_onde.txt', unpack = True)
T = unarray((maxt+mint)/2, (maxt-mint)/2)
f = 1 / T * 1000
f_trig = de2unc(freq_trig, 0., 51/1e4)

fnm, fsd = unpack_unarray(f)

tabella4 = TabularContent()
tabella4.add_column(T)
tabella4.add_column(f)
tabella4.add_column(f_osc)
tabella4.add_column(fnm - f_osc)
tabella4.add_column(f_trig, show_unc = False)
tabella4.add_column(f - f_trig, show_unc = False)
tabella4.save('tabella4.tmp.tex')

# cose finali
latex_file.save('variabili.tmp.tex')

    
