#!/usr/bin/env python3.6
# -*- coding: utf-8 -*-
#
#Oscilloscope Image Cropper
# Luca Arnaboldi - 2019
#
from LabTools.utils.generic import crop_oscilloscope_image

def main(args):
    try:
        crop_oscilloscope_image(args[1])
    except IndexError:
        print('Oh fraaah! Non hai detto che immagine vuoi tagliare!')
    except FileNotFoundError:
        print("Oh fraaah! L'immagine che vuoi tagliare non esiste!")
    except OSError:
        print("Oh fraaah! Non è un immagine!")
    
    

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))
