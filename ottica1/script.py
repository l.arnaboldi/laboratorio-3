import numpy as np
from uncertainties import unumpy as unp
import uncertainties as uncert
import uncertainties.umath as umath

from LabTools.utils import *
from LabTools.latex import *
from LabTools.plot import *
from LabTools.fit import *

latex_file = Document()

"""
Mi aspetto che alpha sia un vettore che contiene nell'ordine(tutti in radianti):
doppietto viola I - 433 nm
turchese I - 486 nm
rosso - 656 nm
doppietto viola II
turchese II

calib invece deve contenere 2 angoli(in radianti) alpha ad ordine 0 ed ordine 1 per la lampada a mercurio
"""
n_2 = np.array([5.,4.,3.,5.,4.])
un_2 = unarray(n_2, [0.]*5)
lambda_mercurio = 546.074e-9

def model(n2, R):
    return 1.e9/(R*(1/2.**2-1./n2**2))

def analize_set(good, calib, alpha, order, tag, outlier = None):
    # Converto la calibrazione in angoli theta
    theta_i = 0.5 * (np.pi - calib[0]) # angolo incidenza
    theta_m = np.pi - theta_i - calib[1] # prim'ordine mercurio
    latex_file.setvariable(UDecimal("thetai"+tag, theta_i))
    latex_file.setvariable(UDecimal("thetam"+tag, theta_m))

    # Calcolo il passo del reticolo
    d = lambda_mercurio/(umath.sin(theta_i)-umath.sin(theta_m))


    sprint(d)
    latex_file.setvariable(UDecimal("d"+tag, d*1e9)) #in nanometri


    theta_d = np.pi - theta_i - alpha
    lambdas = d * (-unp.sin(theta_d) + umath.sin(theta_i)) / order

    tab = TabularContent()
    tab.add_column(alpha)
    tab.add_column(theta_d)
    tab.add_column(order)
    tab.add_column(lambdas*1e9) #in nanometri
    tab.save("tab1"+tag+".tmp.tex")

    good_lambda = lambdas[good] * 1e9 # in nanometri
    if outlier is not None:
        #Sto buttando il violetto di B04
        fit_un_2 = un_2[[0,1,2,4]]
        fit_lambda = good_lambda[[0,1,2,4]]
    else:
        fit_lambda = good_lambda
        fit_un_2 = un_2
    R, = ucurve_fit(model, fit_un_2, fit_lambda)
    chi, ndof = chi2(model, (R,), fit_un_2, fit_lambda)
    latex_file.setvariable(Decimal("chi"+tag, chi))
    latex_file.setvariable(Decimal("ndof"+tag, ndof))
    latex_file.setvariable(UDecimal("R"+tag, R))
    sprint(R)
    sprint(chi)
    sprint(ndof)
    tab = TabularContent()
    tab.add_column(n_2)
    tab.add_column(good_lambda) #in nanometri (lo era già )
    tab.save("tab2"+tag+".tmp.tex")

    residual_plot(
        f = model,
        param = (R,),
        X = un_2,
        Y = good_lambda,
        outliers = outlier,
        xlogscale = False,
        use_ux = False,
        xlabel = '$n_2$',
        ylabel = '$\\lambda$ [$\\si{\\nano\\meter}$]',
        figfile = "grafico"+tag+".pdf",
    )

    lyman = 1./(R*(1-1/4.))
    latex_file.setvariable(UDecimal("lyman"+tag, lyman*1e9)) # in nanometri


def angle2unc(deg, prim, unc):
    return de2unc(deg2rad(deg,)+deg2rad(0., prim), deg2rad(0., unc))

#A06
print("______A06______")
deg, prim, unc = np.loadtxt("dati/A06calib.txt", unpack=True)
data = angle2unc(deg, prim, unc)
beta0 = data[0]
calib = data[1:]
calib = calib + np.pi - beta0

order = np.array([1.,1.,1.,1.,1.,1.,1.,2.,2.,2.])
deg, prim, unc = np.loadtxt("dati/A06.txt",unpack=True)
beta =angle2unc(deg, prim, unc)
alpha = beta - beta0
analize_set([0,2,6,7,9], calib, alpha, order, "Asei")

#B04
print("______B04______")
deg, prim, unc = np.loadtxt("dati/B04calib.txt", unpack=True)
data = angle2unc(deg, prim, unc)
beta0 = data[0] # in realtà è sbagliato!!!
calib = data[1:]

order = np.array([1.,1.,1.,1.,1.,2.,2.])
deg, prim, unc = np.loadtxt("dati/B04.txt",unpack=True)
alpha = angle2unc(deg, prim, unc)
analize_set([0,1,4,5,6], calib, alpha, order, "Bquattro",outlier=[False,False,False,True,False])

#B05
print("______B05______")
rad, unc = np.loadtxt("dati/B05calib.txt", unpack=True)
data = de2unc(rad, unc)
beta0 = data[0]
calib = data[1:]
calib = calib - beta0

order = np.array([1.,1.,1.,1.,1.,2.,2.])
rad, unc = np.loadtxt("dati/B05.txt",unpack=True)
beta = de2unc(rad, unc)
alpha = beta - beta0
analize_set([0,1,4,5,6], calib, alpha, order, "Bcinque")

#B06
print("______B06______")
rad, unc = np.loadtxt("dati/B06calib.txt", unpack=True)
data = de2unc(rad, unc)
beta0 = data[0]
calib = data[1:]
calib = calib - beta0

order = np.array([1.,1.,1.,1.,1.,1.,1.,2.,2.,2.])
rad, unc = np.loadtxt("dati/B06.txt",unpack=True)
beta = de2unc(rad, unc)
alpha = beta - beta0
analize_set([0,2,6,7,9], calib, alpha, order, "Bsei")


# cose finali
latex_file.save('variabili.tmp.tex')
