## Link utili
- [Cartella componenti](https://uz.sns.it/~arna/static_/materiale_universita/Terzo_Anno/Lab3/Componenti/)

## TODO List
- [x] file style LateX
- [x] CI 
- [x] Aggiungere a LabTools plotter che supporta uncertanties
- [x] Fare i fit compatibili con uncertainties
- [ ] Cambiare l'absolute sigma nei fit
- [ ] Multimetro & oscilloscopio
- [ ] Outliers
